#!/bin/bash

#SBATCH --account=def-phaegeli
#SBATCH --time=35:00:00 

#SBATCH --mem=4000M

#SBATCH --output=/home/fherla/jobs/byk2015/logs/byk2015-%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=fherla@sfu.ca

## ---- Init sno ----
date
/home/fherla/jobs/byk2015/cedar_setup.sh
echo "Done init sno"
date
cd /home/fherla/scratch/jobs/byk2015

## ---- Settings for SNOWPACK ----

## Load modules
module load netcdf mariadb

## Specify which version of meteoio/snowpack to run
METEOIO_VERSION=meteoio_784f421
SNOWPACK_VERSION=snowpack_5edfc6c

## Ensure system can find binaries and libraries
export LD_LIBRARY_PATH="$HOME/snowmodels/software/$METEOIO_VERSION/lib":"$HOME/snowmodels/software/$SNOWPACK_VERSION/lib":$LD_LIBRARY_PATH
SNOWPACK="$HOME/snowmodels/software/$SNOWPACK_VERSION/bin/snowpack"



## ---- Execute SNOWPACK ----

## PERIOD A
END_DATE=2014-11-19T00:00Z
$SNOWPACK -c /home/fherla/jobs/byk2015/byk2015A.ini -e $END_DATE 2>&1

## PERIOD B
END_DATE=2014-11-27T12:00Z
$SNOWPACK -c /home/fherla/jobs/byk2015/byk2015B.ini -e $END_DATE 2>&1

## PERIOD C
END_DATE=2015-04-01T00:00Z
$SNOWPACK -c /home/fherla/jobs/byk2015/byk2015C.ini -e $END_DATE 2>&1
