/* ##################################################################


## Sql script to create tables for snowpack output
##
## Table structure corresponds with commit snowpack_1f92aee, eb7b287, 5edfc6c
##
## Databases with this structure:
## - fherla_gnp
## - fherla_byk
## - fherla_s2s
##
## Author: Simon Horton modified by fherla
## Last modified: Mar 2022


################################################################## */

CREATE DATABASE IF NOT EXISTS `fherla_s2s`;
USE `fherla_s2s`;

-- Executions table with info about slurm job
CREATE TABLE IF NOT EXISTS `executions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL DEFAULT current_timestamp(),
  `user_name` varchar(45),
  `comment` varchar(200),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- Profile table with layer properties
CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exec_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `vstation` INT,
  `vstation_id` varchar(45) NOT NULL,
  `angle` float,
  `aspect` float,
  `height` float,
  `deposition_date` datetime,
  `density` int(11),
  `temperature` float,
  `lwc` int(11),
  `grain_size` float,
  `grain_class` int(11),
  `hardness` float,
  `rta` float,
  `sphericity` float,
  `v_strain_rate` float,
  `crit_cut_length` float,
  `shear_strength` float,
  `sk38` float,
  PRIMARY KEY (`id`),
  KEY (`date`, `vstation_id`),
  FOREIGN KEY (`exec_id`) REFERENCES `executions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=8877 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- Timeseries table with weather data
CREATE TABLE IF NOT EXISTS `timeseries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exec_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `vstation` INT,
  `vstation_id` varchar(45) NOT NULL,
  `angle` float,
  `aspect` float,
  `hs` float,
  `hn24` float,
  `hn72` float,
  `wind_transport24` float,
  `hoar` float,
  `tss` float,
  `ski_pen` int(11),
  `iswr` float,
  `ilwr` float,
  `ta` float,
  `rh` float,
  `vw` float,
  `dw` float,
  `psum` float,
  `snow` float,
  `rain` float,
  PRIMARY KEY (`id`),
  KEY (`date`, `vstation_id`),
  FOREIGN KEY (`exec_id`) REFERENCES `executions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=3477 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- Study area table with geospatial data
CREATE TABLE IF NOT EXISTS `studyArea` (
    `vstation` INT NOT NULL,
    `lon` FLOAT,
    `lat` FLOAT,
    `elev` INT,
    `i` INT,
    `j` INT,
	  `band` varchar(45),
	  `region` varchar(45),
    PRIMARY KEY (`vstation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- load grid point information, discarding duplicate vstation entries
LOAD DATA LOCAL INFILE 's2s_gridpoints.csv' 
INTO TABLE `studyArea`
FIELDS TERMINATED BY ',' 
IGNORE 1 ROWS;

EXIT