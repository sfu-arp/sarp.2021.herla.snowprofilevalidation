#!/bin/bash

#SBATCH --account=def-phaegeli
#SBATCH --time=170:00:00

#SBATCH --mem=4000M

#SBATCH --output=/home/fherla/jobs/s2s2017/logs/s2s2017-%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=fherla@sfu.ca

## ---- Init sno ----
date
/home/fherla/jobs/s2s2017/cedar_setup.sh
echo "Done init sno"
date
cd /home/fherla/scratch/jobs/s2s2017

## ---- Settings for SNOWPACK ----

## Load modules
module load netcdf mariadb

## Specify which version of meteoio/snowpack to run
METEOIO_VERSION=meteoio_784f421
SNOWPACK_VERSION=snowpack_5edfc6c

## Ensure system can find binaries and libraries
export LD_LIBRARY_PATH="$HOME/snowmodels/software/$METEOIO_VERSION/lib":"$HOME/snowmodels/software/$SNOWPACK_VERSION/lib":$LD_LIBRARY_PATH
SNOWPACK="$HOME/snowmodels/software/$SNOWPACK_VERSION/bin/snowpack"

## End date for simulations
END_DATE=2017-04-30T12:00Z


## ---- Execute SNOWPACK ----

$SNOWPACK -c /home/fherla/jobs/s2s2017/s2s2017.ini -e $END_DATE 2>&1
