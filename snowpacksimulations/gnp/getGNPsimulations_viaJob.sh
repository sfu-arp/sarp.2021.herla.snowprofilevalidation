#!/bin/bash

#SBATCH --account=def-phaegeli
#SBATCH --time=14:00:00  # expect 10h
#SBATCH --mem-per-cpu=7600M  # should not need more than 7GB for all GNP seasons individually
#SBATCH --ntasks=1
#SBATCH --job-name=getGNPsimulations
#SBATCH --output=/home/fherla/jobs/logs/getGNPsimulations-%j.out
#SBATCH --mail-user=fherla@sfu.ca
#SBATCH --mail-type=BEGIN,END,FAIL

# Author: Florian Herla
# Last modified: June 2021

## With new simulation config (i.e., layer merging OFF, AM & PM output, vslopes) runtime is ~5 times as long
## runtime of 2016/17 season from shorton_gnp (all gridpoints, vslopes) was 2h


module load gcc/9.3.0 r/4.1.0 mariadb
export R_LIBS=$HOME/R_libs
echo R_LIBS=$R_LIBS
echo 
date

echo Starting R script...
Rscript ./getGNPsimulations_viaJob.R
date
