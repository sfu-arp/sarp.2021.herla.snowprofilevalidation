
## ---- Study area ----

library(sp)
library(SarpSnowpackTools)

sa <- readRDS('/home/flo/documents/sfu/code/rpackages_SARP/sarp.2021.herla.snowprofilevalidation/snowpacksimulations/gnp/gnp_studyArea_oldTL.rds')
# sa <- readRDS("~/documents/sfu/code/rpackages_SARP/sarp.2021.herla.snowprofilevalidation/snowpacksimulations/sa_sts_gnp_byk_newest.rds")
par(mfrow = c(1,2))
plot(sa$geometry, main = paste0('HRDPS_WEST (5 seasons, 2012-13 to 2016-17, ', nrow(sa$HRDPS_WEST), ' points)'))
points(sa$HRDPS_WEST, pch = 15, cex = 2, col = terrain.colors(10)[cut(sa$HRDPS_WEST$elev, 9)])
plot(sa$geometry, main = paste0('HRDPS (4 seasons, 2017-18 to 2020-21, ', nrow(sa$HRDPS), ' points)'))
points(sa$HRDPS, pch = 15, cex = 2, col = terrain.colors(10)[cut(sa$HRDPS$elev, 9)])



## ---- Create and modify gnp_gridpoints.csv ----
## this csv file will then be used to create the studyArea table in the fherla_gnp MYSQL DB on cedar

## read all grid points from both NWP models
# GridPoints <- rbind(as.data.frame(sa[["HRDPS"]]), as.data.frame(sa[["HRDPS_WEST"]]))
# # new study Area with 10 km buffer from roads: GridPoints <- rbind(as.data.frame(sa$HRDPS[sa$HRDPS$region == "GLACIER", ]), as.data.frame(sa$HRDPS_WEST[sa$HRDPS_WEST$region == "GLACIER", ]))
# ## for consistency, enforce 6 digits
# GridPoints$vstation <- paste0(ifelse(nchar(GridPoints$vstation) < 6, '0', ''), GridPoints$vstation)
# ## change TL elevation to 2000 +- 100m
# # GridPoints$treeline <- 2000
# GridPoints$band[GridPoints$elev < 1800] <- "BTL"
# GridPoints$band[GridPoints$elev >= 1800] <- "TL"
# GridPoints$band[GridPoints$elev >= 2100] <- "ALP"
# ## export to csv file
# # ExportCols <- type.convert(GridPoints)
# ExportCols <- GridPoints
# ExportCols <- ExportCols[, c("vstation", "lon", "lat", "elev", "i", "j", "band", "zone")]
# ExportCols$zone <- "GNP"
# names(ExportCols) <- c("vstation", "lon", "lat", "elev", "i", "j", "band", "region")
# write.csv(ExportCols, 'snowpacksimulations/gnp/gnp_gridpoints.csv', quote = F, row.names = F)

## ---- Seasonal bias adjustments ----

# ## Load data
# DataDir <- 'H:/My Drive/Academics/SARP/Papers/2020 ProblemTypes/analysis/data'
#
# ## Observations
# obs <- readRDS(file.path(DataDir, '1_Observations.rds'))
#
# ## Model
# Sfc <- readRDS(file.path(DataDir, '2_Sfc.rds'))
# mod <- Sfc[Sfc$elev_band %in% c('TL','BTL'), c('date','elev_band','hs','hn24')]
# #names(mod)[names(mod) == 'qpf_snow_24'] <- 'hn24'
# mod <- reshape(mod, idvar = 'date', timevar = 'elev_band', v.names = c('hs','hn24'), direction = 'wide')
# names(mod) <- paste0(names(mod), '.model')
#
# ## Merge
# hs <- merge(obs, mod, by.x = 'date.obs', by.y = 'date.model')
# names(hs)[names(hs) == 'date.obs'] <- 'date'
# hs$season <- SarpGeneral::deriveAvSeasonFromDate(hs$date)
#
# ## Compute ratio
# hs$factor_fidelity <- hs$hs.TL.obs/hs$hs.TL.model
# hs$factor_rogers <- hs$hs.BTL.obs/hs$hs.BTL.model
# hs_season <- aggregate(factor_fidelity ~ season, hs, median)
# hs_season <- merge(hs_season, aggregate(factor_rogers ~ season, hs, median))
# hs_season[,-1] <- round(hs_season[,-1], 2)
# hs_season
#
# ## 2020-21 season
# #wx2021 <- readRDS('H:/My Drive/Academics/SARP/Papers/2021 Snowfall/2021/data/wx2021_tl.rds')
# #mfs <- wx2021[wx2021$station_id == 'GLACIER_NATIONAL_PARK_MT_FIDELITY',]
# wxd <- readRDS('H:/My Drive/Academics/SARP/Papers/2021 Snowfall/2021/data/wx2021_interp.rds')
# wxd <- wxd[wxd@coords[,2] > 50.5 & wxd@coords[,2] < 52 & wxd@coords[,1] < -116.5 & wxd@coords[,1] > -118.5,]
# wxd$factor <- wxd$hs_obs/wxd$hs_tl
# hs2021 <- aggregate(factor ~ station_id, wxd, median)
# hs2021$factor <- round(hs2021$factor, 2)
# hs2021 <- merge(wxd[!duplicated(wxd$station_id),c('station_id','region')], hs2021)
# hs2021
# hist(hs2021$factor)
# spplot(hs2021, 'factor', pch = 15, cex = 2, sp.layout = list(sa$geometry))
# plot(sa$geometry)
# text(hs2021@coords[,1], hs2021@coords[,2], hs2021$factor)
# hs_season2021 <- data.frame(season = 2021, factor_fidelity = 0.97, factor_rogers = 0.9)
#
# ## Combine
# hs_season <- rbind(hs_season, hs_season2021)
# hs_season$factor <- round(rowMeans(hs_season[,-1]), 2)
# hs_season
#
# ## Save
# saveRDS(hs_season, 'H:/My Drive/Academics/SARP/Research/FloVerification/hs_season.rds')
# hs_season <- readRDS('snowpacksimulations/gnp/hs_season.rds')
# hs_season
# barplot(factor ~ season, hs_season); abline(h = 1)


## ---- Config file ----

## Scripts for generating data for Moses' study live at:
# H:\My Drive\snowmodels\ops\ops2019\snowpackTools\modelRuns\gnp

## Virtual slopes
Vslopes = data.frame(angle = c(38,38,38,38), azi = c(0,90,180,270))
## NOTE: IF MANUAL TEST OF NUMBER_SLOPES = 5 WAS SUCCESSFUL, CORRECT SARPSNOWPACKTOOLS!  -> done!

## Custom info
Config <- ConfigTemplates$NEW

## Desired outputs for ops
Config$OUTPUT$AGGREGATE_MYSQL <- FALSE
Config$OUTPUT$TS_START <- 11 ## Assuming we initialize at 12Z this will write output at 12Z + 11 + 1 = 0Z = 17 PM Mountain Time
Config$OUTPUT$TS_DAYS_BETWEEN <- 1
Config$OUTPUT$PROF_START <- 11
Config$OUTPUT$PROF_DAYS_BETWEEN <- 1
Config$SNOWPACK$ATMOSPHERIC_STABILITY <- "MO_MICHLMAYR"  # I also changed this in defaults in sarpsnowpacktools!
Config$SNOWPACKADVANCED$MEAS_INCOMING_LONGWAVE <- TRUE
Config$SNOWPACKADVANCED$SSI_IS_RTA <- TRUE
Config$SNOWPACKADVANCED$MULTI_LAYER_SK38 <- TRUE

## Scale precip
# Config$FILTERS$`PSUM::FILTER1` <- 'MULT'
# Config$FILTERS$`PSUM::ARG1::TYPE` <- 'CST'

## Environment
EnvInfo <- list(USER = 'fherla',
                METEOIO_VERSION = 'meteoio_784f421',
                SNOWPACK_VERSION = 'snowpack_5edfc6c',
                SNO_SCRIPT = '~/sarpsnowpacktools/snowpack_scripts/initializeSnow.sh')
                # SQL_SCRIPT = '/home/shorton/database_structure.sql')
SqlInfo <- list(DBUSER = 'fherla', DBNAME = 'fherla_gnp', DBPASSWORD = '?If9?ORS:Koookl+')
SlurmInfo <- list(TIME = '30:00:00', MEM = '4000M',
                  NTASKS = 1, NPROCESS  = 1,
                  EMAIL = 'fherla@sfu.ca')

## Loop through seasons
Seasons <- seq(2021, 2022)
for (s in Seasons) {
  print(s)
  StudyName <- paste0('gnp', s)
  # Config$FILTERS$`PSUM::ARG1::CST` <- hs_season$factor[hs_season$season == s]
  try(configSimulation(StudyName = StudyName,
                       StudyArea = sa,
                       Season = s,
                       Input = 'netcdf',
                       Output = 'mysql',
                       EnvInfo = EnvInfo,
                       SlurmInfo = SlurmInfo,
                       SqlInfo = SqlInfo,
                       Vslopes = Vslopes,
                       InitializeSNO = TRUE,
                       InitializeSQL = TRUE,
                       UserConfig = Config,
                       OutDir = '/home/flo/documents/sfu/code/rpackages_SARP/sarp.2021.herla.snowprofilevalidation/snowpacksimulations/gnp/setup_files'))
}



## ---- Cleanup and consolidate files ----

## Remove empty folders
folders <- list.dirs('/home/flo/documents/sfu/code/rpackages_SARP/sarp.2021.herla.snowprofilevalidation/snowpacksimulations/gnp/setup_files', recursive = T)
for(folder in folders) if(length(dir(folder)) == 0) unlink(folder, recursive = T)

## Concat cedar_setup.sh files
setup_files <- list.files('/home/flo/documents/sfu/code/rpackages_SARP/sarp.2021.herla.snowprofilevalidation/snowpacksimulations/gnp/setup_files',
                          'cedar_setup', recursive = T)
setup_cmd <- lapply(setup_files, read.csv, header = F)
setup_cmd <- do.call('rbind', setup_cmd)
write.table(setup_cmd, '/home/flo/documents/sfu/code/rpackages_SARP/sarp.2021.herla.snowprofilevalidation/snowpacksimulations/gnp/setup_files/init_sno.sh',
            sep = ',', row.names = F, col.names = F, quote = F)
lapply(setup_files, unlink)
