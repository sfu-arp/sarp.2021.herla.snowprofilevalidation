/* ##################################################################


## Sql script to create tables for validation data processing database for GNP.
##
## Author: fherla
## Last modified: Feb 2022

Table relations (hierarchy from highest level to lowest level):
1.) Weak layer table `wkl` (i) and `executions` (ii) table
2.) Validation summary table `vframe` (i, ii, iii)
3.) Captured layers table `capturedWKL` (i, ii, iii, iv)
4.) Poor stability layers table `poorWKL` (ii, iv)

- geospatial table `studyArea` (can be joined into all other tables either with `vstation` or `region`)


################################################################## */

CREATE DATABASE IF NOT EXISTS `fherla_wklvalidation`;
USE `fherla_wklvalidation`;

-- Weak layer table
CREATE TABLE IF NOT EXISTS `wkl` (
	`wkl_uuid` varchar(36) NOT NULL,
  `wkl_name` varchar(50) NOT NULL,
  `iscrust` int NOT NULL,  # flag of whether primarily a crust layer
  `SH_only` int NOT NULL,  # flag of whether weak grain type is only SH or a mix of SH/FC
  `season` int NOT NULL,
  `region` varchar(100) NOT NULL,
  `dq` float(2,1) NOT NULL,  # data_quality
  `gsize` int,
  `datetag` date NOT NULL,
  `comment` varchar(250),
  `dtcre` datetime DEFAULT CURRENT_TIMESTAMP,
  `dtmod` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`wkl_uuid`),
  UNIQUE `region_wklname` (`region`, `wkl_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- Executions table
CREATE TABLE IF NOT EXISTS `executions` (
	`exec_uuid` varchar(36) NOT NULL,
	`vdir` int NOT NULL,  # validation direction 1 or 2
	`region` varchar(100) NOT NULL,
	`season` int NOT NULL,
	`band` varchar(45) NOT NULL,
	`flat` varchar(45) NOT NULL,  # flat / vasp
	`pm` varchar(2),  # time of day: PM / AM
	`tw` varchar(45),  # for vdir = 1: wx / fix
	`prcp` varchar(45) NOT NULL,  # 'raw'/'corr'
	`comment` varchar(200),
	`dtcre` datetime DEFAULT CURRENT_TIMESTAMP,
	`dtmod` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`exec_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- Validation table with higher level information
CREATE TABLE IF NOT EXISTS `vframe` (
	`vf_uuid` varchar(36) NOT NULL,  # vframe_uuid
	`exec_uuid` varchar(36) NOT NULL,
	`wkl_uuid` varchar(36) NOT NULL,
	`tmode` varchar(40),  # timing_mode
	`vdate` date NOT NULL,  # validation_date
	`age` int,  # days_after_burial
	`band` varchar(3) NOT NULL,  # while executions.band could include several, this can only be one band!
	`dist` float NOT NULL,  # distribution
	`dist_rep` varchar(20),  # distribution_reported
	`sens_rep` varchar(20),  # sensitivity_reported
	`llhd_rep` float,  # likelihood_typ_reported
	`llhdSprd_rep` float,  # likelihood_spread_reported
	`sz_rep` float,  # size_typ_reported
	`szSprd_rep` float,  # size_spread_reported
	`dng_rep` varchar(20),  # danger_rating_reported
	`gtype_class` varchar(4) NOT NULL,
	`gtype_rank` varchar(10) NOT NULL,
	`gsize_rep` int,  # varies with gtype_class and can't be included in wkl..
	`nSP` int NOT NULL,  # nProfiles_checked
	`twl` int,  # timewindow_lower
	`twu` int,  # timewindow_upper
	`dtmod` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`vf_uuid`),
	-- UNIQUE `vframe_composite` (`wkl_uuid`, `vdate`, `band`, `gtype_rank`, `exec_uuid`),  # this combination of columns makes for unique entries.
	FOREIGN KEY (`exec_uuid`) REFERENCES `executions` (`exec_uuid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	FOREIGN KEY (`wkl_uuid`) REFERENCES `wkl` (`wkl_uuid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	KEY (`gtype_rank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Table with captured critical layers
CREATE TABLE IF NOT EXISTS `captWKL` (
	`capt_id` int NOT NULL AUTO_INCREMENT,  # capturedLayer_id
	`exec_uuid` varchar(36) NOT NULL,
	`wkl_uuid` varchar(36) NOT NULL,
	`vf_uuid` varchar(36) NOT NULL,  # vframe_uuid
	`vdate` date NOT NULL,  # validation_date
	`vstation` INT,
	`vstation_id` varchar(20) NOT NULL,
	`angle` float,
	`aspect` float,
	`height` float,
	`depth` float NOT NULL,
	`thickness` float,
	`ddate` datetime NOT NULL,  # deposition_date
	`bdate` datetime,  # burial_date
	`density` int(11),
	`gsize` float,
	`gtype` varchar(6),
	`hardness` float,
	`sstrength` float,  # shear_strength
	`sk38` float,
	`rc` float,  # crit_cut_length
	`rta` float,
	`pu` float,  # p_unstable
	`sl_rhogs` float,  # slab_rhogs
	`sl_rho` float,
	`tliml` int,  # timelimit_lower
	`tlimu` int,  # timelimit_upper
	`dtmod` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`capt_id`),
	UNIQUE `captWKL_composite` (`vdate`,`vstation_id`,`depth`,`exec_uuid`, `wkl_uuid`, `vf_uuid`),  # exec_uuid encapsulates (pm,tw,prcp)
	FOREIGN KEY (`wkl_uuid`) REFERENCES `wkl` (`wkl_uuid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	FOREIGN KEY (`exec_uuid`) REFERENCES `executions` (`exec_uuid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	KEY (`vf_uuid`)
	-- FOREIGN KEY (`vf_uuid`) REFERENCES `vframe` (`vf_uuid`) ON DELETE RESTRICT ON UPDATE RESTRICT----not enforced, but foreign key implicit!
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- Table with critical layers of poor stability
CREATE TABLE IF NOT EXISTS `poorWKL` (
	`poor_id` int NOT NULL AUTO_INCREMENT,
	`exec_uuid` varchar(36) NOT NULL,
	`vdate` date NOT NULL,
	`nSP` int,  # nProfiles_checked
	`vstation` int,
	`vstation_id` varchar(20) NOT NULL,
	`angle` float,
	`aspect` float,
	`height` float,
	`depth` float NOT NULL,
	`thickness` float,
	`ddate` datetime NOT NULL,
	`bdate` datetime,
	`density` int(11),
	`gsize` float ,
	`gtype` varchar(4),
	`hardness` float,
	`sstrength` float,
	`sk38` float,
	`rc` float,
	`rta` float,
	`pu` float,
	`sl_rhogs` float,
	`sl_rho` float,
	`dtmod` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`poor_id`),
	FOREIGN KEY (`exec_uuid`) REFERENCES `executions` (`exec_uuid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	UNIQUE `poorWKL_composite` (`vdate`,`vstation_id`,`depth`,`exec_uuid`)  # exec_uuid encapsulates (pm,prcp)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



-- Study area table with geospatial data
CREATE TABLE IF NOT EXISTS `studyArea` (
  `vstation` INT NOT NULL,
  `lon` FLOAT DEFAULT NULL,
  `lat` FLOAT DEFAULT NULL,
  `elev` INT DEFAULT NULL,
  `i` INT DEFAULT NULL,
  `j` INT DEFAULT NULL,
	`band` varchar(45) CHARACTER SET utf8 NOT NULL,
	`region` varchar(45) CHARACTER SET utf8 NOT NULL,
	`dtcre` datetime DEFAULT CURRENT_TIMESTAMP,
	`dtmod` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`vstation`),
  INDEX (`region`)
);


-- load grid point information, discarding duplicate vstation entries
LOAD DATA LOCAL INFILE 'gnp_gridpoints.csv'
INTO TABLE `studyArea`
FIELDS TERMINATED BY ','
IGNORE 1 ROWS
SET dtcre=NOW();

LOAD DATA LOCAL INFILE 'byk_gridpoints.csv'
INTO TABLE `studyArea`
FIELDS TERMINATED BY ','
IGNORE 1 ROWS
SET dtcre=NOW();

LOAD DATA LOCAL INFILE 's2s_gridpoints.csv'
INTO TABLE `studyArea`
FIELDS TERMINATED BY ','
IGNORE 1 ROWS
SET dtcre=NOW();

-- load WklList into wkl table with R script `insertWKL2DB.R`..
