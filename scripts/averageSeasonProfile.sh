#!/bin/bash

#SBATCH --account=def-phaegeli
#SBATCH --time=5:00:00
#SBATCH --mem-per-cpu=14800  # 7600  # (GNP) 2017 needs more memory!  # 14800 for S2S (?), 11700 for BYK (?)
#SBATCH --ntasks=1
#SBATCH --job-name=averageSeasonProfile
#SBATCH --output=averageSeasonProfile-%j.out
#SBATCH --mail-user=fherla@sfu.ca
#SBATCH --mail-type=FAIL,BEGIN,END

# The following job script will start sarp.2021.herla.snowprofilevalidation::averageSeasonProfile.R
#
# Author: Florian Herla
# Last modified: Apr 2022

## CHECKLIST before running on cedar
# * all (SARP) packages installed with recent version?
# * Snow_DB contains all required profiles?
# * relevant outdirs exist?

####################

module load python/3.7 scipy-stack/2021a r/4.1.0
export R_LIBS=$HOME/R_libs

Rscript ./averageSeasonProfile.R
