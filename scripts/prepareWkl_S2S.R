## Analogon to prepareWkl_GNP.R
##
## fherla Nov 2022

library(SarpBulletinTools)

## load object that contains a list of these years which in turn
## contains the Bulletin as well as WklList for each year.
## origin: AvalX AvCan data base and Avid DB
load("/home/flo/documents/sfu/code/rpackages_SARP/sarp.2021.herla.snowprofilevalidation/data/220128_ExtractWkl/S2S/S2S_WklInfo_AllSeasons.RData")

## create one giant WklList
giant <- do.call("c", lapply(BullData, function(bu) bu$WklInfo))
class(giant) <- "WklList"

## useful subfunction
as_WklList <- function(WklElement) {
  class(WklElement) <- "WklList"
  return(WklElement)
}


## interactive commands to display the relevant information
# i <- i + 1
#
# paste(i, "=", names(giant[i]))
# giant[[i]]$ASSESS[, c("ASSESS_DATETIME", "COMMENTS")]
# giant[[i]]$AVPROB$Tl[, c("CHARACTER", "COMMENT")]
# paste(i, "=", names(giant[i]))
# summary(as_WklList(giant[i]))
#
# View(giant[[i]]$ASSESS)
# View(giant[[i]]$AVPROB$Alp)
# View(giant[[i]]$AVPROB$Tl)
# View(giant[[i]]$AVPROB$Btl)
#
# paste(i, "=", names(giant[i]))
# summary(as_WklList(giant[i]))


## --- manipulation prompts ----
## vector that stores which layers to discard finally
toDiscard <- c(2, 3, 4, 8,
               14
)

## --- 2019 ----
giant$`2019.Dec 25/26 SH`$dataqualityFH <- 4
giant$`2019.Dec 25/26 SH`$commentFH <- "Never heard of again"
giant$`2019.Jan 17 SH`$dataqualityFH <- 2.3
giant$`2019.Jan 17 SH`$commentFH <- "No evidence of reactivity"
giant$`2019.Jan. 31 CR/FC/SH`$ASSESS$GRAIN_TYPE <- "SH"
giant$`2019.Jan. 31 CR/FC/SH`$ASSESS$GRAIN_TYPE_SECONDARY <- "FC"
giant$`2019.Jan. 31 CR/FC/SH`$ASSESS$GRAIN_TYPE_TERTIARY <- "MFcr"
giant$`2019.Jan. 31 CR/FC/SH`$dataqualityFH <- 3.1
giant$`2019.Jan. 31 CR/FC/SH`$commentFH <- "Very shortlived instability"



## --- 2018 ----
giant$`2018.Dec 14 MFcr`$ASSESS$GRAIN_TYPE <- "MFcr"
giant$`2018.Dec 14 MFcr`$dataqualityFH <- 3.3
giant$`2018.Dec 14 MFcr`$commentFH <- "Purely speculative, never reactive"
giant$`2018.Dec 27 CR/FC/SH Mix`$ASSESS$GRAIN_TYPE <- "FC"
giant$`2018.Dec 27 CR/FC/SH Mix`$ASSESS$GRAIN_TYPE_SECONDARY <- "SH"
giant$`2018.Dec 27 CR/FC/SH Mix`$ASSESS$GRAIN_TYPE_TERTIARY <- "MFcr"
giant$`2018.Dec 27 CR/FC/SH Mix`$dataqualityFH <- 3.3
giant$`2018.Dec 27 CR/FC/SH Mix`$commentFH <- "Speculated for brief reactivity"
giant$`2018.Jan 6 Crust`$ASSESS$GRAIN_TYPE <- "MFcr"
giant$`2018.Jan 6 Crust`$dataqualityFH <- 3.3
giant$`2018.Jan 6 Crust`$commentFH <- "Only brief storm problem"
giant$`2018.Jan 15 Crust`$ASSESS$GRAIN_TYPE <- "MFcr"
giant$`2018.Jan 15 Crust`$dataqualityFH <- 2.2
giant$`2018.Jan 15 Crust`$commentFH <- "Brief concern: low prob high consequ"
giant$`2018.Feb 22 Mix`$ASSESS$GRAIN_TYPE <- "SH"
giant$`2018.Feb 22 Mix`$ASSESS$GRAIN_TYPE_SECONDARY <- "FC"
giant$`2018.Feb 22 Mix`$ASSESS$GRAIN_TYPE_TERTIARY <- "IFsc"
giant$`2018.Feb 22 Mix`$dataqualityFH <- 2.2
giant$`2018.Feb 22 Mix`$commentFH <- "Of concern, reactive, poor test results, low prob high consequ"
giant$`2018.March 21`$ASSESS$GRAIN_TYPE <- "SH"
giant$`2018.March 21`$ASSESS$GRAIN_TYPE_SECONDARY <- "FC"
giant$`2018.March 21`$ASSESS$GRAIN_TYPE_TERTIARY <- "MFcr"
giant$`2018.March 21`$dataqualityFH <- 1
giant$`2018.March 21`$commentFH <- "Of concern, reactive"


## --- 2017 ----
giant$`2017.Nov 12 or 13`$ASSESS$GRAIN_TYPE <- "MFcr"
giant$`2017.Nov 12 or 13`$dataqualityFH <- 3.3
giant$`2017.Nov 12 or 13`$commentFH <- "Anticipated instability rather speculative, never got reactive"
giant$`2017.Mid-February CR`$ASSESS$GRAIN_TYPE <- "IFrc"
giant$`2017.Mid-February CR`$ASSESS$MAX_ELEV <- 2200
giant$`2017.Mid-February CR`$dataqualityFH <- 2.2
giant$`2017.Mid-February CR`$commentFH <- "Of concern, reactive"
giant$`2017.Feb 25 Facets & More`$dataqualityFH <- 2.2
giant$`2017.Feb 25 Facets & More`$commentFH <- "Of concern, isolated, briefly reactive"
giant$`2017.Mar 15 MFcr`$ASSESS$GRAIN_TYPE <- "IFrc"
giant$`2017.Mar 15 MFcr`$dataqualityFH <- 3.3
giant$`2017.Mar 15 MFcr`$commentFH <- "Only ever storm snow problem for 1 day"



## --- final execution prompts ----
## discard non-persistant layers
giant <- giant[-(toDiscard)]



## --- add 2020-2022 seasons from Avid DB ----
load("data/221107_ExtractWkl/S2S/S2S_WklInfo_AllSeasons.RData")

## create one giant WklList
avid <- do.call("c", lapply(BullData, function(bu) bu$WklInfo))
yearBool <- do.call("c", do.call("c", lapply(BullData, function(bu) sapply(bu$WklInfo, function(el) SarpGeneral::deriveAvSeasonFromDate(el$BURIAL_DATE) %in% c(2020, 2021, 2022)))))
avid <- avid[yearBool]
class(avid) <- "WklList"

# i <- i + 1
#
# paste(i, "=", names(avid[i]))
# summary(as_WklList(avid[i]))
#

## --- 2022 ----
avid$`2022.Feb 14 Cr/Fc`$ASSESS$GRAIN_TYPE <- "FC"
avid$`2022.Feb 14 Cr/Fc`$ASSESS$GRAIN_TYPE_TERTIARY <- "MFcr"
avid$`2022.Feb 14 Cr/Fc`$ASSESS$MAX_ELEV <- 2100
avid$`2022.Feb 14 Cr/Fc`$dataqualityFH <- 1
avid$`2022.Feb 14 Cr/Fc`$commentFH <- "Of concern, reactive, very consistent"
avid$`2022.Early December MFcr/FC/SH`$ASSESS$GRAIN_TYPE <- "MFcr"
avid$`2022.Early December MFcr/FC/SH`$ASSESS$GRAIN_TYPE_TERTIARY <- "DH"
avid$`2022.Early December MFcr/FC/SH`$dataqualityFH <- 2.1
avid$`2022.Early December MFcr/FC/SH`$commentFH <- "inconsistent: FC/SH might have formed in dry spell after MFcr. of concern, reactive"
avid$`2022.Jan 29 MFcr/FC/SH`$ASSESS$GRAIN_TYPE <- "MFcr"
avid$`2022.Jan 29 MFcr/FC/SH`$ASSESS$GRAIN_TYPE_TERTIARY <- "DH"
avid$`2022.Jan 29 MFcr/FC/SH`$dataqualityFH <- 1
avid$`2022.Jan 29 MFcr/FC/SH`$commentFH <- "of concern, reactive, consistent"
avid$`2022.Late-March/Early-April Crust`$ASSESS$GRAIN_TYPE <- "MFcr"
avid$`2022.Late-March/Early-April Crust`$dataqualityFH <- 3.1
avid$`2022.Late-March/Early-April Crust`$commentFH <- "Only reactive during storm loading"
avid$`2022.Nov 15 MFCr`$ASSESS$GRAIN_TYPE <- "MFcr"
avid$`2022.Nov 15 MFCr`$dataqualityFH <- 3.1
avid$`2022.Nov 15 MFCr`$commentFH <- "Very little information"


## --- 2021 ----
avid$`2021.Jan 24`$ASSESS$GRAIN_TYPE <- "FC"
avid$`2021.Jan 24`$ASSESS$GRAIN_TYPE_SECONDARY <- "SH"
avid$`2021.Jan 24`$ASSESS$GRAIN_TYPE_TERTIARY <- "MFcr"
avid$`2021.Jan 24`$dataqualityFH <- 2.2
avid$`2021.Jan 24`$commentFH <- "Not much information about reactivity"
avid$`2021.Dec 8-10 melt freeze crust`$ASSESS$GRAIN_TYPE <- "MFcr"
avid$`2021.Dec 8-10 melt freeze crust`$ASSESS$GRAIN_TYPE_TERTIARY <- "DH"
avid$`2021.Dec 8-10 melt freeze crust`$ASSESS$MAX_ELEV <- 2200
avid$`2021.Dec 8-10 melt freeze crust`$dataqualityFH <- 2.1
avid$`2021.Dec 8-10 melt freeze crust`$commentFH <- "inconsistent: SH/FC formed in dry spell after MFcr! otherwise of concern, very reactive"



## --- 2020 ----
avid$`2020.Dec 25 SH`$ASSESS$GRAIN_TYPE <- "SH"
avid$`2020.Dec 25 SH`$dataqualityFH <- 4
avid$`2020.Dec 25 SH`$commentFH <- "No comments, 1 storm snow problem"
avid$`2020.Dec 10 SH`$ASSESS$GRAIN_TYPE <- "SH"
avid$`2020.Dec 10 SH`$ASSESS$GRAIN_TYPE_SECONDARY <- "FC"
avid$`2020.Dec 10 SH`$dataqualityFH <- 4
avid$`2020.Dec 10 SH`$commentFH <- "Hypothesized, 1 storm snow problem"
avid$`2020.Feb 21 SH/FCsf`$ASSESS$GRAIN_TYPE <- "SH"
avid$`2020.Feb 21 SH/FCsf`$ASSESS$GRAIN_TYPE_SECONDARY <- "FC"
avid$`2020.Feb 21 SH/FCsf`$dataqualityFH <- 2.3
avid$`2020.Feb 21 SH/FCsf`$commentFH <- "Of concern for >10 days, but seemingly unreactive"
avid$`2020.Nov 23 CR/FC`$ASSESS$GRAIN_TYPE <- "FC"
avid$`2020.Nov 23 CR/FC`$ASSESS$GRAIN_TYPE_TERTIARY <- "MFcr"
avid$`2020.Nov 23 CR/FC`$dataqualityFH <- 1
avid$`2020.Nov 23 CR/FC`$commentFH <- "Unusually persistent and reactive, prime data point"
avid$`2020.Dec 16 SH`$ASSESS$GRAIN_TYPE <- "SH"
avid$`2020.Dec 16 SH`$ASSESS$MAX_ELEV <- 2200
avid$`2020.Dec 16 SH`$dataqualityFH <- 3.1
avid$`2020.Dec 16 SH`$commentFH <- "massively buried, healed quickly"

## --- final execution prompts ----
out <- c(giant, avid)
class(out) <- "WklList"

## make sure there are no hypostrophes in the WKL names
## (it will be removed in MYSQL DB and then the names of the WKL entities don't match anymore)
names(out) <- gsub("'", "", names(out))

## remove all avprobs except storm, (deep) persistent  | update: done in PWLcapturedByModel.R

## save WklList
# saveRDS(out, file = "data/WklList_S2S_2017-2022.rds")


