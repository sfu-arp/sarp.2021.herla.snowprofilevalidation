## This script analyzes the proportion of grid points that structurally captured or missed WKLs
## (1) CTree analyses
## (2) marginal distributions stratified by different variables

## --- initializations ----
library(sarp.2021.herla.snowprofilevalidation)
library(partykit)

## read data set that contains detailed assessments of captured and missed layers of concern VS
VS <- readRDS("output/evaluate_main/S2S_BYK_GNP_v06/captWKLAssessment.rds")
## read ValidationData VD
VD <- readRDS("output/evaluate_main/S2S_BYK_GNP_v06/mergedVDvframe.rds")
## format VS
simGsizeAtBurial <- readRDS("output/evaluate_main/S2S_BYK_GNP_v06gs/simulatedGsizeAtBurial.rds")
VS <- format_captWKLquality(VS, VD, simulatedGsizeAtBurial = simGsizeAtBurial)




## --- specific stratification to play with ----
## Mind the 'double-counting' of WKL instances:
## If not explicitely excluded below, then there is one WKL data point per
## band (ALP, TL, BTL), and per applicable gtype_rank (primary, secondary, tertiary)
## Hence, each WKL can contribute to up to 6 data points in this analysis.
VS_ <- VS
## e.g., exclude tertiary grain type rank
VS_ <- VS_[VS_$gtype_rank != "tertiary", ]




## --- ctrees ----
## response: pcapt_max
## potential additional explanatory variables:
## * drySpell_std
## * gsizeAtBurial (attention: might include substantial number of NAs!): simulated gsize at burial [not included in present data set yet!]
## * gsize_rep (attention: might include substantial number of NAs!): reported gsize at burial

## full tree
VS_ <- VS
CT <- partykit::ctree(pcapt_max ~ region + season + band + dq + gtype_rank + gtype_class + wkl_iscrust + nPDays + wklmonth + drySpell_median + gsizeAtBurial_median + gsize_rep, data = VS_, alpha = 0.05)
plot(CT, terminal_panel = node_violinplot)

## full tree simplified
png("output/figures/paper/ctree_propcaptured.png", width = 1000, height = 500)
VS_ <- VS
VS_ <- VS_[!VS_$gtype_class %in% c("DH", "FCxr"), ]
VS_$gtype_class <- as.character(VS_$gtype_class)
VS_$gtype_class[(VS_$gtype_class == "SH" & VS_$gtype_rank == "secondary")] <- "SH/FC"
VS_$gtype_class <- as.factor(VS_$gtype_class)
CT <- partykit::ctree(pcapt_max ~ gtype_class + drySpell_median + dq + wklmonth, data = VS_, alpha = 0.05, maxdepth = 3)
# plot(CT, terminal_panel = node_violinplot)
Vars <- setVars4ctreeColoring(VS_)
plotCTree(CT)
dev.off()
## TAKE AWAY (additional to full tree)
## * SH substantially less likely to be captured if length of dry spell is shorter than 3 days (only shown at maxdepth == 4!) [nWKL = 3]


## zoom into crusts: i.e., RHS of full tree:
VS_ <- VS
VS_ <- VS_[VS_$gtype_class %in% c("IFrc", "IFsc", "MFcr"), ]
CT <- partykit::ctree(pcapt_max ~ season + region + band + dq + gtype_rank + gtype_class + wkl_iscrust + nPDays + wklmonth + drySpell_median, data = VS_, alpha = 0.05)
plot(CT, terminal_panel = node_violinplot)
## TAKE AWAY:
## * Early season rain and temperature crusts are well captured at BTL
## *  ~/~ on average moderately well captured at ALP and TL, but with huge spread
## * Mid- and late-season rain and temperature crusts are yet less well captured in all bands, also huge spread
## * Sun crusts are basically not captured at all (the BTL terminal node )


## zoom into weak grains: i.e., LHS of full tree:
VS_ <- VS
VS_ <- VS_[!VS_$gtype_class %in% c("IFrc", "IFsc", "MFcr"), ]
CT <- partykit::ctree(pcapt_max ~ region + season + band + dq + gtype_rank + gtype_class + wkl_iscrust + nPDays + wklmonth + drySpell_median + gsizeAtBurial_median + gsize_rep, data = VS_, alpha = 0.05)
plot(CT, terminal_panel = node_violinplot)
## TAKE AWAY:
## * Length of dry spell preceeding the burial is a driving factor: almost all WKLs with a dry spell longer than 7 days exist is almost all grid points
## * If dry spell is less than 7 days:
##   - FC are almost always captured by large majority of grid points
##   - FC are also almost always present next to SH in GNP/BYK even if they were not reported
##   - If SH or DH reported
##      > strict grain type search routine: huge spread
##   -  > relaxed routine: captured by majority of grid points











## --- marginal distributions ----
## stratified by gtype
VS_ <- VS
VS_$gtype_class <- factor(VS_$gtype_class)
vioplot::vioplot(pcapt_max ~ gtype_class, data = VS_)
abline(h = seq(0, 1, by = 0.2), lty = "dashed", col = "gray70")
for (cl in seq(length(levels(VS_$gtype_class)))) {
  mtext(paste0("n = ", nrow(VS_[VS_$gtype_class == levels(VS_$gtype_class)[cl], ])), at = cl)
}

## stratified by gtype search routine (strict = primary, relaxed = secondary)
VS_ <- VS
VS_ <- VS_[!VS_$gtype_class %in% c("IFrc", "IFsc", "MFcr"), ]
VS_$gtype_rank <- factor(VS_$gtype_rank)
vioplot::vioplot(pcapt_max ~ gtype_rank, data = VS_)
abline(h = seq(0, 1, by = 0.2), lty = "dashed", col = "gray70")

## stratified by data quality
# VS_$dq <- factor(VS_$dq)
# vioplot::vioplot(pcapt_max ~ dq, data = VS_)
# abline(h = seq(0, 1, by = 0.2), lty = "dashed", col = "gray70")
# for (cl in seq(length(levels(VS_$dq)))) {
#   mtext(paste0("n = ", nrow(VS_[VS_$dq == levels(VS_$dq)[cl], ])), at = cl)
# }

## "which layers are those?"
## e.g., SH layers present in less than 40 % of grid points:
VS_ <- VS
poor_wid <- unique(VS_$wkl_uuid[VS_$pcapt_max < 0.4 & VS_$gtype_class == "SH"])
View(VD$wkl[VD$wkl$wkl_uuid %in% poor_wid, ])
## essentially: only two SH layers of concern are missing in entire data set; all other missing SH layers have 'good excuses' (see WKL comments in table)

