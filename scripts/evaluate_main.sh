#!/bin/bash

#SBATCH --account=def-phaegeli
#SBATCH --time=8:00:00
#SBATCH --mem-per-cpu=11600
#SBATCH --ntasks=1
#SBATCH --job-name=evaluate
#SBATCH --output=%x-%j.out
#SBATCH --mail-user=fherla@sfu.ca
#SBATCH --mail-type=FAIL,BEGIN,END

# The following job script will start sarp.2021.herla.snowprofilevalidation::evaluate_main.R
#
# Author: Florian Herla
# Last modified: August 2022

## CHECKLIST before running on cedar
# * all (SARP) packages installed with recent version?
# * wklvalidation database contains all validation data?


####################

module load r/4.1.0
export R_LIBS=$HOME/R_libs

Rscript ./evaluate_main.R
