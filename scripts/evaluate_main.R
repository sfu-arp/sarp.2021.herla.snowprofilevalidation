## This script assesses / evaluates the entire results stored in the wklvalidation data base, i.e.
##    * retrieves data from the wklvalidation DB for desired regions/seasons/bands/gtype_ranks
##    * performs the regional layer matching check (by function call `transactPoor2CaptWKL`)
##    * merges all VD$vframe and $wkl tables of all DB retrievals (this data format is necessary for detailed analyses later)
##    * computes the confusion matrix data.frame that will yield the performance curves
##    * (also computes the confusion matrix data.frame at the grid point level for a potential spatial analysis that has not been carried out yet) [only for HRDPS grid points, season >= 2018!]
##    * calls `assessQualityOfcaptWKL`to compute correlations and other agreement indicators of all captured and missed layers of concern


## OUTPUT:
##    * one rds file containing the resulting data.frame from `assessQualityOfcaptWKL` in 'outdir' (specify below)
##    * one rds file for the confusion matrix data.frame (and one for the spatial data frame, if season >= 2018)
##    * one rds file for the merged VD$vframe & $wkl tables
##    * several figures (for each season, band) for a quick visual interpretation of how well the model did in each season

## SUBSEQUENT ANALYSIS:
## all files produced by this job can be further analyzed in detail by the scripts "analyze_*.R"


## --- CONFIG ----
REGIONS <- c("S2S", "BYK", "GNP")
SEASONS <- c(seq(2013, 2022))
BANDS <- c("BTL", "TL", "ALP")
GTYPE_RANKS <- c("secondary", "tertiary", "primary")


config <- list()
config$stabilityMeasure <- "processbasedInst"  # either "p_unstable" (i.e., statistical approach) or "processbasedInst" (i.e., process-based approach)
config$runname_version <- "v06.2"

## semi-automatic config of runnames and output directories:
config$runname <- paste0(paste0(REGIONS, collapse = "_"), "_", config$runname_version,
                         ifelse(config$stabilityMeasure == "processbasedInst", "_pbInst", ""))
config$outdir <- paste0("/home/fherla/project/fherla/snowprofilevalidation/output/evaluate_main/", config$runname, "/")
config$outdirfigures <- paste0(config$outdir, "figures/")
if (!dir.exists(config$outdir) & Sys.getenv("USER") != "flo") dir.create(config$outdir)
if (!dir.exists(config$outdirfigures) & Sys.getenv("USER") != "flo") dir.create(config$outdirfigures)
config$DB <- "Snow_DB"  # actually, this script was never really designed to be run on Steed_DB, but only on Snow_DB (= cedar)!


## manual config of run settings:
config$evaluateAspects <- FALSE  # TRUE not implemented yet!

config$transactPoor2CaptWKLs <- TRUE  # TRUE: move captWKL to poorWKL based on datetag check on the regionally grouped level

config$mergeVDvframe <- list(fname = "mergedVDvframe",
                             outdir = config$outdir,
                             add_gsizeAtBurial = FALSE,  # takes a LOT of cpu time!
                             add_timewindows = TRUE,  # takes quite a bit cpu time!
                             add_poorWKLgtypeclass = TRUE
                             )

config$deriveConfusionMatrixFrame <- list(fname = "confusionMatrix",
                                          outdir = config$outdir
                                          )
# config$deriveConfusionMatrixFrame <- FALSE

# config$deriveConfusionMatrixAtGridpoints <- list(fname = "confusionMatrix_gridpoints",
#                                                  outdir = config$outdir
#                                                  )  # only for HRDPS grid points, season >= 2018
config$deriveConfusionMatrixAtGridpoints <- FALSE

config$assessQualityOfcaptWKL <- list(fname = "captWKLAssessment",
                                      outdir = config$outdir,
                                      plotFig = FALSE,  # file: 'DngLlhdWklPuAvg_regionseason_band_gtyperank'
                                      avgSPdir = "/home/fherla/project/fherla/snowprofilevalidation/output/averageProfiles/",
                                      GTYPE_RANK = c("secondary", "tertiary", "primary")  ## deprecated, but first element still defines plotFig!
                                      )
# config$assessQualityOfcaptWKL <- FALSE

# config$plotWXpotentialDatetags <- list(outdir = config$outdirfigures,
#                                        avgSPdir = "/home/fherla/project/fherla/snowprofilevalidation/output/averageProfiles/")
config$plotWXpotentialDatetags <- FALSE





## settings that are changed semi-automatically/semi-manually
if (config$stabilityMeasure == "p_unstable") {
  config$poorWKLconstraint <- "pu >= 0.77 AND gtype NOT IN ('PP', 'DF')"
} else if (config$stabilityMeasure == "processbasedInst") {
  config$poorWKLconstraint <- "rta >= 0.8 AND sk38 <= 1.0 AND rc <= 0.3 AND gtype NOT IN ('PP', 'DF')"
  config$deriveConfusionMatrixAtGridpoints <- FALSE
}
if (Sys.getenv("USER") == "fherla") config$DB <- "Snow_DB"
message(paste0("Database set to ", config$DB))
## common on local and cedar:
datapath <- paste0(system.file("data", package = "sarp.2021.herla.snowprofilevalidation"), "/")
options(warn = 1, nwarnings = 5000)
message(paste0("options('warn') = ", options("warn")))
message(paste0("stability measure set to: ", config$stabilityMeasure))




## --- INIT ----
library(sarp.2021.herla.snowprofilevalidation)
library(SarpGeneralVis)
library(SarpBulletinTools)
library(sarp.internal.snowpack)
## get Bulletins
## either by downloading them (unfortunately this regularly fails when cedar's network is heavily used during the day),
## or by reading a local file uploaded to cedar
load("/home/fherla/project/fherla/snowprofilevalidation/data/AllBulletins_2010To2022.RData")
# download.attempt <- 1
# try({suppressWarnings(load(url("http://data.avalancheresearch.ca/AllBulletins_2010To2022.RData")))})
# while (!exists("Bulletins")) {
#   if (download.attempt == 3) stop("Cannot download Bulletins, looks like there are server or network issues. Try again later.")
#   if (download.attempt == 2) Sys.sleep(100)
#   Sys.sleep(20)
#   download.attempt <- download.attempt + 1
#   try({suppressWarnings(load(url("http://data.avalancheresearch.ca/AllBulletins_2010To2022.RData")))})
# }
# Bulletins <- readRDS(paste0(datapath, "AllBulletins_2010To2022.rds"))  # requires file to be installed with package (i.e., only at fherla's laptop)
## read Renviron file for DB interaction variables
suppressWarnings(readRenviron(path = "/home/flo/documents/sfu/code/cedarstuff/CedarTunnel.Renviron"))
suppressWarnings(readRenviron("/home/fherla/Cedar.Renviron"))
port <- as.integer(Sys.getenv(paste0(config$DB, "_Tunnel_Port")))
DBCon <- SarpGeneral::tryMultipleTimes(RMySQL::dbConnect(DBI::dbDriver("MySQL"),
                                                         host = Sys.getenv(paste0(config$DB, "_Host")),
                                                         user = Sys.getenv(paste0(config$DB, "_Un")),
                                                         password = Sys.getenv(paste0(config$DB, "_Pw")),
                                                         dbname = "fherla_wklvalidation",
                                                         port = ifelse(is.na(port), 0, port)),
                                       MaxNumTries = 3)

## define subfunctions
ensureDailySampling <- function(meta, data = NULL) {
  meta$discard <- FALSE
  dbls <- which(duplicated(meta[, c("station_id", "date")]))
  for (stid in unique(meta$station_id[dbls])) {
    if (any(duplicated(meta[meta$station_id == stid, c("date", "station_id")]))) {
      meta$discard[meta$station_id == stid][which(duplicated(meta[meta$station_id == stid, c("date", "station_id")])) - 1] <- TRUE
    }
  }
  if (!is.null(data)) data <- data[!meta$discard]
  meta <- meta[!meta$discard, ]
  if (!is.null(data)) {
    return(list(profiles=data, meta=meta))
  } else {
    return(meta)
  }
}
rollingsum.cumsum <- function(x, n = 3L) cumsum(x) - cumsum(c(rep(0, n), head(x, -n)))

message(paste0(rep("#", times  = 100), collapse = ""))

## on local laptop 'steed': read csv tables containing study area grid points
# try({CMgp <- rbind(read.csv("snowpacksimulations/gnp/gnp_gridpoints.csv"),
#               read.csv("snowpacksimulations/byk/byk_gridpoints.csv"),
#               read.csv("snowpacksimulations/s2s/s2s_gridpoints.csv"))})
CMgp <- DBI::dbReadTable(DBCon, "studyArea")
CMgp[, c("p", "pmod", "n", "tp", "fp", "sensitivity", "specificity", "precision",
         "p_raw", "pmod_raw", "n_raw", "tp_raw", "fp_raw", "sensitivity_raw", "specificity_raw", "precision_raw")] <- NA
CM <- data.frame()
QU <- data.frame()
VDVF <- list()

for (region in REGIONS) {
  for (season in SEASONS) {

    if (region == "S2S" & season %in% seq(2013, 2016)) next  # S2S does not have any validation data in these seasons!

    ## get Bulletins
    BullUUIDs <- suppressWarnings(Bulletins$Bulletins$BULLETIN_ID[Bulletins$Bulletins$SEASON %in% season & Bulletins$Bulletins$REGION %in% RegionNamesList[[region]]])
    Bull <- extractFromBulletinObj(Bulletins, ByBulletinID = BullUUIDs)

    for (band in BANDS) {
      message(paste0("\n", Sys.time(), ": ", region, " ", season, ", ", band))

      for (gtype_rank in GTYPE_RANKS) {
        if (gtype_rank == "secondary") gtype_rank <- c("secondary", "primary")
        options(warn = 1, nwarnings = 5000)
        message(paste0("gtype_rank: ", paste0(gtype_rank, collapse = ", ")))

        ## retrieve VD from DB fherla_wklvalidation
        VD <- getVdata(DBCon = DBCon, DB = config$DB, season = season, band = band, region = region, poorWKL = TRUE, captWKL = TRUE,
                          poorWKLconstraint = config$poorWKLconstraint, gtype_rank = gtype_rank)
        # VD <- calculateProportionUnstable(VD, target = c("vframe", "poorWKL"))  # need VD$poorWKL$datetags first..


      ## --- derive and assign simulated datetags ----
        message("Retrieving and aggregating weather variables")
        ## Note this step is only possible since we store the weather data daily (instead of e.g. hourly) --> otherwise aggregate RAIN temporally first!!
        WX <- getWeather(tolower(region), AddDem = TRUE, TimeDiffUTC = -7,
                         DateStart = paste0(as.character(season-1), "-09-01"), DateEnd = paste0(as.character(season), "-05-30"),
                         FlatOnly = TRUE, DailyTime = NA,
                         SizeCheck = FALSE, Verbose = FALSE,
                         Variables = c("hn24", "hn72", "rain"))
        WX <- WX[WX$band == band, ]
        WX <- ensureDailySampling(WX)
        WX <- WX[order(WX$station_id, WX$datetime), ]
        WX$rain72 <- unlist(lapply(unique(WX$station_id), function(sid) rollingsum.cumsum(WX$rain[WX$station_id == sid])))
        HN24 <- aggregate(hn24 ~ date, data = WX, FUN = function(x) quantile(x, probs = 0.75))
        HN72 <- aggregate(hn72 ~ date, data = WX, FUN = function(x) quantile(x, probs = 0.25))
        RAIN24 <- aggregate(rain ~ date, data = WX, FUN = function(x) quantile(x, probs = 0.75))
        RAIN72 <- aggregate(rain72 ~ date, data = WX, FUN = function(x) quantile(x, probs = 0.25))
        WX24 <- data.frame(date = sort(unique(WX$date)))
        for (dt in unique(WX24$date)) {
          WX24$hn24[WX24$date == dt] <- HN24$hn24[HN24$date == dt]
          WX24$hn72[WX24$date == dt] <- HN72$hn72[HN72$date == dt]
          WX24$rain24[WX24$date == dt] <- RAIN24$rain[RAIN24$date == dt]
          WX24$rain72[WX24$date == dt] <- RAIN72$rain72[RAIN72$date == dt]
        }
        WX <- NULL
        PDT <- derivePotentialDatetags(WX24, VD$wkl$datetag)  # required for confusion matrix compilation!
        # VD$poorWKL <- assignLayer2Datetag(VD$poorWKL, unique(c(PDT, VD$wkl$datetag)))  # done in transact.. below

        if (!isFALSE(config$plotWXpotentialDatetags) & band == "TL" & gtype_rank[1] == "secondary") {
          if (!dir.exists(config$plotWXpotentialDatetags$outdir) & Sys.getenv("USER") != "flo") dir.create(config$plotWXpotentialDatetags$outdir)
          ## get avgSP  (1) try correct band, (2) try TL, (3) resign
          avgSPpath <- paste0(config$plotWXpotentialDatetags$avgSPdir, region, season, "_", band, ".rds")
          avgSPbol <- file.exists(avgSPpath)
          if (avgSPbol) {
            avgSP <- readRDS(avgSPpath)
          } else {
            avgSPpath <- paste0(config$assessQualityOfcaptWKL$avgSPdir, region, season, "_TL.rds")
            avgSPbol <- file.exists(avgSPpath)
            if (avgSPbol) {
              avgSP <- readRDS(avgSPpath)
            } else {
              avgSP <- NULL
            }
          }
          pngpath <- paste0(config$plotWXpotentialDatetags$outdir, "WX_", region, season, "_", band, ".png")
          message(paste0("Saving figure ", pngpath))
          if (!is.null(avgSP)) {
            png(pngpath, width = 1400, height = 1200)
          } else {
            png(pngpath, width = 1400, height = 800)
          }
          plotWXandPotentialDatetags(WX24, PDT, VD, avgSP)
          dev.off()
        }


        ## --- transaction poorWKLs to captWKLs ----
        if (config$transactPoor2CaptWKLs) {
          message("Transacting poor and capt WKL databases")
          VD <- transactPoor2CaptWKL(VD, WX24, PDT)
        }

        ## --- merge VD vframe after transacted layers and after calculating proportions _captured & _unstable ----
        if (!isFALSE(config$mergeVDvframe)) {
          message("Merging VD$vframe from past iterations")
          VDVF <- mergeVDvframe(VDVF, VD,
                                add_gsizeAtBurial = config$mergeVDvframe$add_gsizeAtBurial,
                                add_timewindows = config$mergeVDvframe$add_timewindows,
                                add_poorWKLgtypeclass = config$mergeVDvframe$add_poorWKLgtypeclass,
                                region = region, band = band, gtype_rank = gtype_rank)
          saveRDS(VDVF, paste0(config$mergeVDvframe$outdir, config$mergeVDvframe$fname, ".rds"))
        }


        ## --- deriveConfusionMatrixFrame ----
        if (!isFALSE(config$deriveConfusionMatrixFrame)) {
          message("Deriving confusion matrix data frame")
          ## captWKLs
          TMPc <- VD$wkl[, c("wkl_uuid", "wkl_name", "iscrust", "season", "region", "dq", "datetag")]
          if (nrow(TMPc) > 0) {
            TMPc$nPDays <- sapply(TMPc$wkl_uuid, function(wid) {
              length(VD$vframe$tmode[VD$vframe$wkl_uuid == wid & !is.na(VD$vframe$tmode)])
            })
            TMPc$nPDays_mod <- sapply(TMPc$wkl_uuid, function(wid) {
              length(VD$vframe$vdate[VD$vframe$wkl_uuid == wid & VD$vframe$proportion_unstable >= 0.05])
            })
            TMPc$forecasted <- TRUE
            TMPc$pcapt_max <- sapply(TMPc$wkl_uuid, function(wid) {
              suppressWarnings(max(VD$vframe$dist[VD$vframe$wkl_uuid == wid & !is.na(VD$vframe$tmode)], na.rm = TRUE))
            })
            TMPc$pu_max <- sapply(TMPc$wkl_uuid, function(wid) {
              suppressWarnings(max(VD$vframe$proportion_unstable[VD$vframe$wkl_uuid == wid & !is.na(VD$vframe$tmode)], na.rm = TRUE))
            })
          }
          ## poorWKLs
          datetags <- as.Date(unique(VD$poorWKL$datetag))
          nAdd <- length(datetags)
          if (nAdd > 0) {
            TMPp <- data.frame(datetag = datetags,
                               pu_max = sapply(datetags, function(tag) length(unique(VD$poorWKL$vstation_id[VD$poorWKL$datetag == tag])) / max(VD$poorWKL$nSP[VD$poorWKL$datetag == tag])),
                               nPDays_mod = sapply(datetags, function(tag) length(unique(VD$poorWKL$vdate[VD$poorWKL$datetag == tag & VD$poorWKL$proportion_unstable >= 0.05])))
            )
            TMPp$pcapt_max <- NA
            TMPp$nPDays <- NA
            TMPp$forecasted <- FALSE
            TMPp$season <- season
            TMPp$region <- region
            TMPp$wkl_uuid <- NA
            TMPp$wkl_name <- NA
            TMPp$iscrust <- NA
            TMPp$dq <- NA
            TMP <- rbind(TMPc, TMPp)
          } else {
            TMP <- TMPc
            TMPp <- TMP[integer(),]
          }
          ## Approach: allPDTs
          datetags2add <- PDT[!PDT %in% c(TMPc$datetag, TMPp$datetag)]
          ndt2add <- length(datetags2add)
          if (ndt2add > 0) {
            CM2add <- TMP[integer(), ]
            CM2add[seq(length(datetags2add)), ] <- NA
            CM2add$datetag <- datetags2add
            CM2add[, c("season", "nPDays_mod", "pu_max")] <-
              c(rep(season, times = ndt2add), rep(0, times = ndt2add), rep(0, times = ndt2add))
            CM2add[, c("region")] <- rep(region, times = ndt2add)
            CM2add[, c("forecasted")] <- rep(FALSE, times = ndt2add)
            if (nrow(TMP) > 0) {
              TMP <- rbind(TMP, CM2add)
            } else {
              TMP <- CM2add
            }
          }

          ## generic
          if (nrow(TMP) > 0) {
            TMP$gtype_rank <- gtype_rank[1]
            TMP$band <- band
          }

          CM <- rbind(CM, TMP)
          ## save CM in each iteration (i.e., simple alternative to tryCatch..)
          saveRDS(CM, paste0(config$deriveConfusionMatrixFrame$outdir, config$deriveConfusionMatrixFrame$fname, ".rds"))

          ## --- deriveConfusionMatrixAtGridpoints ----
          ## not implemented for process-based instability; p_unstable is hardcoded!
          if (!isFALSE(config$deriveConfusionMatrixAtGridpoints) & season >= 2018) {
            message("Deriving confusion matrix data frame for individual grid points")
            if (config$evaluateAspects) stop("This analysis cannot be rightfully performed on aspects!")
            if (nrow(TMP) > 0) {
              ## apply filterings:
              ## discard nPDays == 0
              toDiscard <- which(TMP$nPDays == 0)
              if (length(toDiscard) > 0) TMP <- TMP[-toDiscard, ]
              ## discard all poor layers with human datetags (remainders of transact routine)
              toDiscard <- which(!TMP$forecasted & TMP$datetag %in% TMP$datetag[TMP$forecasted])
              if (length(toDiscard) > 0) TMP <- TMP[-toDiscard, ]

              ## raw computations
              for (i in which(CMgp$region == region & CMgp$band == band)) {
                CMgp$p_raw[i] <- sum(CMgp$p_raw[i], sum(TMP$forecasted, na.rm = TRUE), na.rm = TRUE)
                CMgp$n_raw[i] <- sum(CMgp$n_raw[i], sum(!TMP$forecasted, na.rm = TRUE), na.rm = TRUE)
                forecasted_datetags <- as.character(unique(TMP$datetag[TMP$forecasted]))
                if (length(forecasted_datetags) > 0) {
                  CMgp$tp_raw[i] <- sum(CMgp$tp_raw[i],
                                        sum(sapply(forecasted_datetags, function(dt) length(which(VD$captWKL$vstation %in% CMgp$vstation[i]
                                                                                                  & VD$captWKL$angle == 0  # only flat field!!
                                                                                                  & VD$captWKL$wkl_uuid %in% VD$wkl$wkl_uuid[VD$wkl$datetag == dt]
                                                                                                  & VD$captWKL$pu >= 0.77)) > 0)),
                                        na.rm = TRUE)
                } else {
                  CMgp$tp_raw[i] <- 0
                }
                modeled_datetags <- as.character(unique(TMP$datetag))
                CMgp$pmod_raw[i] <- sum(CMgp$pmod_raw[i],
                                      sum(c(sapply(modeled_datetags, function(dt) length(which(VD$captWKL$vstation %in% CMgp$vstation[i]
                                                                                                & VD$captWKL$angle == 0  # only flat field!!
                                                                                                & VD$captWKL$wkl_uuid %in% VD$wkl$wkl_uuid[VD$wkl$datetag == dt]
                                                                                                & VD$captWKL$pu >= 0.77)) > 0),
                                            sapply(modeled_datetags, function(dt) length(which(VD$poorWKL$vstation %in% CMgp$vstation[i]
                                                                                                      & VD$poorWKL$angle == 0  # only flat field!!
                                                                                                      & VD$poorWKL$datetag == dt
                                                                                                      & VD$poorWKL$pu >= 0.77)) > 0))),
                                      na.rm = TRUE)
                ## specificity TNR = 1 - FPR = 1 - FP/N
                ## FP = modeled but not forecasted
                non_forecasted_datetags <- as.character(unique(TMP$datetag[!TMP$forecasted]))
                CMgp$fp_raw[i] <- sum(CMgp$fp_raw[i],
                                  sum(sapply(non_forecasted_datetags, function(dt) length(which(VD$poorWKL$vstation %in% CMgp$vstation[i]
                                                                                         & VD$poorWKL$angle == 0  # only flat field!!
                                                                                         & VD$poorWKL$datetag == dt
                                                                                         & VD$poorWKL$pu >= 0.77)) > 0)),
                                  na.rm = TRUE)
                CMgp$sensitivity_raw <- CMgp$tp_raw / CMgp$p_raw
                CMgp$specificity_raw <- 1 - (CMgp$fp_raw /CMgp$n_raw)
                CMgp$precision_raw <- CMgp$tp_raw / CMgp$pmod_raw
              }  # END raw spatial computations
              ## filterings for high DQ and secondary gtype_rank
              if ("secondary" %in% gtype_rank) {
                CMgp$p[i] <- sum(CMgp$p[i], sum(TMP$forecasted[TMP$dq <= 2.2], na.rm = TRUE), na.rm = TRUE)
                CMgp$n[i] <- sum(CMgp$n[i], sum(!TMP$forecasted, na.rm = TRUE), na.rm = TRUE)
                forecasted_datetags <- as.character(unique(TMP$datetag[TMP$forecasted & TMP$dq <= 2.2]))
                if (length(forecasted_datetags) > 0) {
                  CMgp$tp[i] <- sum(CMgp$tp[i],
                                    sum(sapply(forecasted_datetags, function(dt) length(which(VD$captWKL$vstation %in% CMgp$vstation[i]
                                                                                              & VD$captWKL$angle == 0  # only flat field!!
                                                                                              & VD$captWKL$wkl_uuid %in% VD$wkl$wkl_uuid[VD$wkl$datetag == dt]
                                                                                              & VD$captWKL$pu >= 0.77)) > 0)),
                                    na.rm = TRUE)
                } else {
                  CMgp$tp[i] <- 0
                }

                modeled_datetags <- as.character(unique(TMP$datetag))
                CMgp$pmod[i] <- sum(CMgp$pmod[i],
                                        sum(c(sapply(modeled_datetags, function(dt) length(which(VD$captWKL$vstation %in% CMgp$vstation[i]
                                                                                               & VD$captWKL$angle == 0  # only flat field!!
                                                                                               & VD$captWKL$wkl_uuid %in% VD$wkl$wkl_uuid[VD$wkl$datetag == dt]
                                                                                               & VD$captWKL$pu >= 0.77)) > 0),
                                              sapply(modeled_datetags, function(dt) length(which(VD$poorWKL$vstation %in% CMgp$vstation[i]
                                                                                                        & VD$poorWKL$angle == 0  # only flat field!!
                                                                                                        & VD$poorWKL$datetag == dt
                                                                                                        & VD$poorWKL$pu >= 0.77)) > 0))),
                                        na.rm = TRUE)
                ## specificity TNR = 1 - FPR = 1 - FP/N
                ## FP = modeled but not forecasted
                non_forecasted_datetags <- as.character(unique(TMP$datetag[!TMP$forecasted]))
                CMgp$fp[i] <- sum(CMgp$fp[i],
                                  sum(sapply(non_forecasted_datetags, function(dt) length(which(VD$poorWKL$vstation %in% CMgp$vstation[i]
                                                                                                & VD$poorWKL$angle == 0  # only flat field!!
                                                                                                & VD$poorWKL$datetag == dt
                                                                                                & VD$poorWKL$pu >= 0.77)) > 0)),
                                  na.rm = TRUE)
                CMgp$sensitivity <- CMgp$tp / CMgp$p
                CMgp$specificity <- 1 - (CMgp$fp /CMgp$n)
                CMgp$precision <- CMgp$tp / CMgp$pmod
              }  # END high DQ secondary computations

              ## save CMgp in each iteration (i.e., simple alternative to tryCatch..)
              saveRDS(CMgp, paste0(config$deriveConfusionMatrixAtGridpoints$outdir, config$deriveConfusionMatrixAtGridpoints$fname, ".rds"))

            }}  # END deriveConfusionMatrixAtGridPoints
        }  # END deriveConfusionMatrix


        ## --- assessQualityOfcaptWKL ----
        message("Starting assessQualityOfcaptWKL")
        if (!isFALSE(config$assessQualityOfcaptWKL)) {

          ## rbind QU data.frames from all iterations
          QU <- rbind(QU,
                      assessQualityOfcaptWKL(VD, band = band, stabilityindex = "p_unstable")
          )
          QU <- QU[order(QU$wkldate), ]

          ## save QU in each iteration (i.e., simple alternative to tryCatch..)
          saveRDS(QU, paste0(config$assessQualityOfcaptWKL$outdir, config$assessQualityOfcaptWKL$fname, ".rds"))

          ## plotTSDngLlhdWklPuAvg
          if (config$assessQualityOfcaptWKL$plotFig & gtype_rank[1] == config$assessQualityOfcaptWKL$GTYPE_RANK[1]) {
            ## get avgSP  (1) try correct band, (2) try TL, (3) resign
            avgSPpath <- paste0(config$assessQualityOfcaptWKL$avgSPdir, region, season, "_", band, ".rds")
            avgSPbol <- file.exists(avgSPpath)
            if (avgSPbol) {
              avgSP <- readRDS(avgSPpath)
            } else {
              avgSPpath <- paste0(config$assessQualityOfcaptWKL$avgSPdir, region, season, "_TL.rds")
              avgSPbol <- file.exists(avgSPpath)
              if (avgSPbol) {
                avgSP <- readRDS(avgSPpath)
              } else {
                avgSP <- NULL
              }
            }
            ## figure to compare DngR, Likelihood, proportion_unstable, avgSP  <--> correlation table
            pngpath <- paste0(config$outdirfigures, "DngLlhdWklPuAvg_", region, season, "_", band, "_", gtype_rank[1], ".png")
            message(paste0("Saving figure ", pngpath))
            png(filename = pngpath, width = 1800, height = 1200)
            try({plotTSDngLlhdWklPuAvg(VD, Bull, avgSP, band)})
            dev.off()  # commented out due to erroneous dev.off() in plotTSDng.. function!
          }  # END IF plot composite figure

        }  # END IF assessQualityOfcaptWKL

        ## check integrity of VDVF
        if (!(all(VDVF$vframe$wkl_uuid %in% QU$wkl_uuid))) warning("VDVF is missing wkl_uuids!")

        warnings()  # this seems to be useless..countless googleing didn't help.. options(warn = 1) should do the trick, but it doesn't on cedar with the Rscript command?!
      }  # END LOOP gtype_rank
    } # END LOOP band
  }  # END LOOP season
}  # END LOOP region

RMySQL::dbDisconnect(DBCon)
rm(DBCon)
