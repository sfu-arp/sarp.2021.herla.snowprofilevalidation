#!/bin/bash

#SBATCH --account=def-phaegeli
#SBATCH --time=35:00:00
#SBATCH --mem-per-cpu=7600  # 2017 season needs 11600!
#SBATCH --ntasks=1
#SBATCH --job-name=validateBYK
#SBATCH --output=%x-%j.out
#SBATCH --mail-user=fherla@sfu.ca
#SBATCH --mail-type=FAIL,BEGIN,END

# The following job script will start sarp.2021.herla.snowprofilevalidation::validateBYK_main.R
#
# Author: Florian Herla
# Last modified: August 2022

## CHECKLIST before running on cedar
# * all (SARP) packages installed with recent version?
# * created wklvalidation database?
#   * loaded DB with studyArea?
#   * loaded WklList into wkl table?
# * Snow_DB contains all required profiles?

####################

module load python/3.7 scipy-stack/2021a r/4.1.0
export R_LIBS=$HOME/R_libs

Rscript ./validateBYK_main.R
