## This script was used to initially derive the methods for grouping simulated unstable layers by model-derived date tags.
## Will keep for now, maybe contains some useful perspectives/approaches for future tasks.

library(sarp.2021.herla.snowprofilevalidation)
library(zoo)

# getVdata..
# VD <- readRDS("~/documents/sfu/code/rpackages_SARP/sarp.2021.herla.snowprofilevalidation/data/VData_GNP_2013_BTL_ranks2.rds")
VD <- readRDS("~/documents/sfu/code/rpackages_SARP/sarp.2021.herla.snowprofilevalidation/data/VData_GNP_2019_TL_ranks12.rds")

avgSP <- readRDS("output/averageProfiles/GNP2019_TL.rds")

## --- assign tags/labels to poorWKL ----

## daily histogram breaks and pre-defined cut-off thresholds:
## unique bdates from poorWKL:
ubd <- sort(unique(c(as.Date(VD$poorWKL$bdate), as.Date(VD$captWKL$bdate))))


## poorWKLs
bdates <- as.Date(as.character(VD$poorWKL$bdate))
Saturdays <- seq(min(ubd), max(ubd), 1)[weekdays(seq(min(ubd), max(ubd), 1), abbreviate = FALSE) == "Saturday"]
SaturdayLabels <- format(seq(min(ubd), max(ubd), 1)[weekdays(seq(min(ubd), max(ubd), 1), abbreviate = FALSE) == "Saturday"], "%b %d")
## first bdates generally
png("output/figures/poorWKL_labelling/bdate_histograms_GNP_2019_TL_poorWKL_ranks12_transact.png", width = 1440, height = 1140)
par(cex.axis = 1.4, cex.lab = 1.4)
# par(mfrow = c(4, 1))
layout(matrix(seq(4), ncol = 1), heights = c(1, 1, 1, 2))
H <- hist(as.Date(VD$poorWKL$bdate), breaks = seq(min(ubd), to = max(ubd), by = 1), freq = TRUE, ylab = "total bdate occurrence", col = "lightgray", xlim = c(min(ubd), max(ubd)), xaxt = "n",
     xlab = "", main = "All unstable layers that ARE NOT associated with tracked PWLs (DB 'poorWKL')\n Total occurrence")
axis(1, at = Saturdays, labels = SaturdayLabels, las = 2)
axis(2, at = pretty(H$counts))
abline(v = as.Date(VD$wkl$datetag), lty = "dashed")
legend("topright", "datetags of tracked PWLs", lty = "dashed", cex = 1.2)
## then temporal component:
H <- hist(as.Date(unlist(lapply(unique(bdates), function(bd) rep(as.character(bd), times = length(unique(VD$poorWKL$vdate[bdates == bd])))))), breaks = seq(min(ubd), to = max(ubd), by = 1), freq = TRUE,
     ylab = "number of problematic days", col = "lightgray", xlim = c(min(ubd), max(ubd)), ylim = c(0, 60), xaxt = "n", xlab = "", main = "Temporal occurrence")
axis(1, at = Saturdays, labels = SaturdayLabels, las = 2)
axis(2, at = pretty(H$counts))
abline(v = as.Date(VD$wkl$datetag), lty = "dashed")
abline(h = c(20, 40, 60), lty = "dotted", col = "lightgray")
## then spatial component:
H <- hist(as.Date(unlist(lapply(unique(bdates), function(bd) rep(as.character(bd), times = 100*length(unique(VD$poorWKL$vstation[bdates == bd])) / max(VD$poorWKL$nSP[bdates == bd]) )))),
     breaks = seq(min(ubd), to = max(ubd), by = 1), freq = TRUE, ylab = "proportion of problematic grid cells (%)", col = "lightgray", xlim = c(min(ubd), max(ubd)), ylim = c(0, 100), xaxt = "n", xlab = "", main = "Spatial occurrence")
axis(1, at = Saturdays, labels = SaturdayLabels, las = 2)
axis(2, at = pretty(H$counts))
abline(v = as.Date(VD$wkl$datetag), lty = "dashed")
abline(h = c(20, 40, 60, 80, 100), lty = "dotted", col = "lightgray")
## avgSP
plot(avgSP$avgs[avgSP$meta$date >= min(ubd) & avgSP$meta$date <= max(ubd)], main = "average profile GNP 2019")
abline(v = as.Date(VD$wkl$datetag), lty = "dashed")
dev.off()


## captWKLs (with poor stability)
bdates <- as.Date(as.character(VD$captWKL$bdate[VD$captWKL$pu >= 0.77]))
## first bdates generally
png("output/figures/poorWKL_labelling/bdate_histograms_GNP_2019_TL_captWKL_ranks12_transact.png", width = 1440, height = 1140)
par(cex.axis = 1.4, cex.lab = 1.4)
# par(mfrow = c(3, 1))
layout(matrix(seq(4), ncol = 1), heights = c(1, 1, 1, 2))
H <- hist(bdates, breaks = seq(min(ubd), to = max(ubd), by = 1), freq = TRUE, ylab = "bdate occurrence general", col = "lightgray", xlim = c(min(ubd), max(ubd)), xaxt = "n",
          xlab = "", main = "All unstable captured layers that ARE associated with tracked PWLs (DB 'captWKL')\n Total occurrence")
axis(1, at = Saturdays, labels = SaturdayLabels, las = 2)
axis(2, at = pretty(H$counts))
abline(v = as.Date(VD$wkl$datetag), lty = "dashed")
legend("topright", "datetags of tracked PWLs", lty = "dashed", cex = 1)
## then temporal component:
H <- hist(as.Date(unlist(lapply(unique(bdates), function(bd) rep(as.character(bd), times = length(unique(VD$captWKL$vdate[VD$captWKL$bdate == bd & VD$captWKL$pu >= 0.77])))))), breaks = seq(min(ubd), to = max(ubd), by = 1), freq = TRUE,
          ylab = "number of problematic days", col = "lightgray", xlim = c(min(ubd), max(ubd)), ylim = c(0, 60), xaxt = "n", xlab = "", main = "Temporal occurrence")
axis(1, at = Saturdays, labels = SaturdayLabels, las = 2)
axis(2, at = pretty(H$counts))
abline(v = as.Date(VD$wkl$datetag), lty = "dashed")
abline(h = c(20, 40, 60), lty = "dotted", col = "lightgray")
## then spatial component:
H <- hist(as.Date(unlist(lapply(unique(bdates), function(bd) rep(as.character(bd), times = 100*length(unique(VD$captWKL$vstation[VD$captWKL$bdate == bd & VD$captWKL$pu >= 0.77])) / max(VD$vframe$nSP[VD$vframe$vf_uuid %in% VD$captWKL$vf_uuid[bdates == bd]], na.rm = TRUE) )))),
          breaks = seq(min(ubd), to = max(ubd), by = 1), freq = TRUE,
          ylab = "proportion of problematic grid cells (%)", col = "lightgray", xlim = c(min(ubd), max(ubd)), ylim = c(0, 100), xaxt = "n", xlab = "", main = "Spatial occurrence")
axis(1, at = Saturdays, labels = SaturdayLabels, las = 2)
axis(2, at = pretty(H$counts))
abline(v = as.Date(VD$wkl$datetag), lty = "dashed")
abline(h = c(20, 40, 60, 80, 100), lty = "dotted", col = "lightgray")
## avgSP
plot(avgSP$avgs[avgSP$meta$date >= min(ubd) & avgSP$meta$date <= max(ubd)], main = "average profile GNP 2019")
abline(v = as.Date(VD$wkl$datetag), lty = "dashed")
dev.off()






## --- derive HN24 thresholds 4 datetags ----
library(sarp.internal.snowpack)
suppressWarnings(readRenviron(path = "/home/flo/documents/sfu/code/cedarstuff/CedarTunnel.Renviron"))
region <- "GNP"
season <- 2019
band <- "TL"
WX <- getWeather(tolower(region), AddDem = TRUE, TimeDiffUTC = -7,
                 DateStart = paste0(as.character(season-1), "-09-01"), DateEnd = paste0(as.character(season), "-05-30"),
                 FlatOnly = TRUE, DailyTime = NA,
                 SizeCheck = FALSE, Verbose = TRUE,
                 Variables = c("hn24", "hn72"))
WX <- WX[WX$band == band, ]
ensureDailySampling <- function(meta, data = NULL) {
  meta$discard <- FALSE
  dbls <- which(duplicated(meta[, c("station_id", "date")]))
  for (stid in unique(meta$station_id[dbls])) {
    if (any(duplicated(meta[meta$station_id == stid, c("date", "station_id")]))) {
      meta$discard[meta$station_id == stid][which(duplicated(meta[meta$station_id == stid, c("date", "station_id")])) - 1] <- TRUE
    }
  }
  if (!is.null(data)) data <- data[!meta$discard]
  meta <- meta[!meta$discard, ]
  if (!is.null(data)) {
    return(list(profiles=data, meta=meta))
  } else {
    return(meta)
  }
}
WX <- ensureDailySampling(WX)
HN24 <- aggregate(hn24 ~ date, data = WX, FUN = median)
HN72 <- aggregate(hn72 ~ date, data = WX, FUN = median)
WX24 <- data.frame(date = sort(unique(WX$date)))
for (dt in unique(WX24$date)) {
  WX24$hn24[WX24$date == dt] <- HN24$hn24[HN24$date == dt]
  WX24$hn72[WX24$date == dt] <- HN72$hn72[HN72$date == dt]
}

PDT <- derivePotentialDatetags(WX24)

Saturdays <- seq(min(ubd), max(ubd), 1)[weekdays(seq(min(ubd), max(ubd), 1), abbreviate = FALSE) == "Saturday"]
SaturdayLabels <- format(seq(min(ubd), max(ubd), 1)[weekdays(seq(min(ubd), max(ubd), 1), abbreviate = FALSE) == "Saturday"], "%b %d")
par(mfrow = c(4, 1))
plot(as.Date(HN24$date), HN24$hn24, type = "l", main = "HN24", axes = FALSE, xlab = "", ylab = "")
axis(1, at = Saturdays, labels = SaturdayLabels, las = 2)
abline(h = c(5, 10, 15), lty = "dotted")
abline(v = PDT, col = "gray70", lwd = 2)
abline(v = as.Date(VD$wkl$datetag), lty = "dotted", col = "red")

plot(as.Date(HN24$date), cumsum(HN24$hn24), type = "l", main = "HN24", axes = FALSE, xlab = "", ylab = "")
axis(1, at = Saturdays, labels = SaturdayLabels, las = 2)
abline(h = c(5, 10, 15), lty = "dotted")
abline(v = PDT, col = "gray70", lwd = 2)
abline(v = as.Date(VD$wkl$datetag), lty = "dotted", col = "red")

plot(as.Date(HN72$date), HN72$hn72, type = "l", main = "HN72", axes = FALSE, ylab = "", xlab = "")
axis(1, at = Saturdays, labels = SaturdayLabels, las = 2)
abline(h = c(5, 10, 15), lty = "dotted")
abline(v = PDT, col = "gray70", lwd = 2)
abline(v = as.Date(VD$wkl$datetag), lty = "dotted", col = "red")

plot(as.Date(HN72$date), cumsum(HN72$hn72), type = "l", main = "HN72", axes = FALSE, ylab = "", xlab = "")
axis(1, at = Saturdays, labels = SaturdayLabels, las = 2)
abline(h = c(5, 10, 15), lty = "dotted")
abline(v = PDT, col = "gray70", lwd = 2)
abline(v = as.Date(VD$wkl$datetag), lty = "dotted", col = "red")

VD$poorWKL$datetag <- assignLayer2Datetag(VD$poorWKL, PDT)$datetag

## histogram of poorWKLs assigned to datetags
datetags <- VD$poorWKL$datetag
Saturdays <- seq(min(ubd), max(ubd), 1)[weekdays(seq(min(ubd), max(ubd), 1), abbreviate = FALSE) == "Saturday"]
SaturdayLabels <- format(seq(min(ubd), max(ubd), 1)[weekdays(seq(min(ubd), max(ubd), 1), abbreviate = FALSE) == "Saturday"], "%b %d")
## first bdates generally
png("output/figures/poorWKL_labelling/datetag_histograms_GNP_2019_TL_poorWKL_ranks12_transact.png", width = 1440, height = 1140)
par(cex.axis = 1.4, cex.lab = 1.4)
# par(mfrow = c(4, 1))
layout(matrix(seq(4), ncol = 1), heights = c(1, 1, 1, 2))
H <- hist(datetags, breaks = seq(min(datetags, ubd), to = max(ubd), by = 1), freq = TRUE, ylab = "total datetag occurrence", col = "lightgray", xlim = c(min(ubd), max(ubd)), xaxt = "n",
          xlab = "", main = "All unstable layers that ARE NOT associated with tracked PWLs (DB 'poorWKL')\n Total occurrence")
axis(1, at = Saturdays, labels = SaturdayLabels, las = 2)
axis(2, at = pretty(H$counts))
abline(v = as.Date(VD$wkl$datetag), lty = "solid", col = "red")
abline(v = PDT, lty = "dashed")
# legend("topright", "datetags of tracked PWLs", lty = "dashed", cex = 1.2)
## then temporal component:
H <- hist(as.Date(unlist(lapply(unique(datetags), function(bd) rep(as.character(bd), times = length(unique(VD$poorWKL$vdate[datetags == bd])))))), breaks = seq(min(ubd, datetags), to = max(ubd), by = 1), freq = TRUE,
          ylab = "number of problematic days", col = "lightgray", xlim = c(min(ubd), max(ubd)), ylim = c(0, 30), xaxt = "n", xlab = "", main = "Temporal occurrence")
axis(1, at = Saturdays, labels = SaturdayLabels, las = 2)
axis(2, at = pretty(H$counts))
abline(v = as.Date(VD$wkl$datetag), lty = "solid", col = "red")
abline(v = PDT, lty = "dashed")
abline(h = c(5, 10, 20), lty = "dotted", col = "lightgray")
## then spatial component:
H <- hist(as.Date(unlist(lapply(unique(datetags), function(bd) rep(as.character(bd), times = 100*length(unique(VD$poorWKL$vstation[datetags == bd])) / max(VD$poorWKL$nSP[datetags == bd]) )))),
          breaks = seq(min(ubd, datetags), to = max(ubd), by = 1), freq = TRUE, ylab = "proportion of problematic grid cells (%)", col = "lightgray", xlim = c(min(ubd), max(ubd)), ylim = c(0, 100), xaxt = "n", xlab = "", main = "Spatial occurrence")
axis(1, at = Saturdays, labels = SaturdayLabels, las = 2)
axis(2, at = pretty(H$counts))
abline(v = as.Date(VD$wkl$datetag), lty = "solid", col = "red")
abline(v = PDT, lty = "dashed")
abline(h = c(20, 40, 60, 80, 100), lty = "dotted", col = "lightgray")
## avgSP
plot(avgSP$avgs[avgSP$meta$date >= min(ubd) & avgSP$meta$date <= max(ubd)], main = "average profile GNP 2019")
abline(v = as.Date(VD$wkl$datetag), lty = "solid", col = "red")
abline(v = PDT, lty = "dashed")
dev.off()



## --- number of problematic days from captWKL ----
hist(sapply(unique(VD$vframe$wkl_uuid), function(wid) {
  nrow(VD$vframe[VD$vframe$wkl_uuid == wid & !is.na(VD$vframe$tmode), ])
}))


## --- time-bubbles and proportion histogram capt & poor ----
VD <- readRDS("~/documents/sfu/code/rpackages_SARP/sarp.2021.herla.snowprofilevalidation/data/VData_GNP_2019_TL_ranks12.rds")
WX24 <- readRDS("data/WX24_GNP_2019_TL.rds")
VD <- transactPoor2CaptWKL(VD, WX24)
VD$captWKL$datetag <- as.Date(sapply(VD$captWKL$wkl_uuid, function(wid) VD$wkl$datetag[VD$wkl$wkl_uuid == wid]))
avgSP <- readRDS("output/averageProfiles/GNP2019_TL.rds")


## -- bubbles
## TODO...

## -- histogram
ubd <- sort(unique(c(as.Date(VD$poorWKL$bdate), as.Date(VD$captWKL$bdate))))
png("output/confusionMatrix/figures/histogram_datetags_poor_capt_GNP_2019_TL_ranks12_b.png", 1200, height = 900)
par(mfrow = c(2, 1), cex = 1.8, cex.lab = 1.5, mar = c(4.1, 6.1, 4.1, 2.1))

## avgSP
if (!is.null(avgSP)) {
  ## make ppu_all avail for plotting
  if (!"percentage" %in% names(avgSP$avgs[[1]]$layers)) {
    avgSP$avgs <- snowprofileSet(lapply(avgSP$avgs, function(avg) {
      avg$layers$percentage <- avg$layers$ppu_all
      avg
    }))
  }

  plot(avgSP$avgs[avgSP$meta$date >= min(ubd) & avgSP$meta$date <= max(ubd)], colAlpha = 0.5, xaxs = "i", yaxs = "i", box = FALSE, ylab = "")
  plot(avgSP$avgs[avgSP$meta$date >= min(ubd) & avgSP$meta$date <= max(ubd)], ColParam = "percentage", add = TRUE, box = FALSE, ylab = "")
  mtext("Height (cm)", side = 2, line = 3, cex = 2.2)
  abline(v = unique(as.Date(VD$poorWKL$datetag)), lty = "dashed", lwd = 3)
  abline(v = unique(as.Date(VD$captWKL$datetag)), lty = "dashed", col = "red", lwd = 3)
}
par(mar = c(4.1, 6.1, 2.1, 2.1))
## POORWKL
Saturdays <- seq(min(ubd), max(ubd), 1)[weekdays(seq(min(ubd), max(ubd), 1), abbreviate = FALSE) == "Saturday"]
SaturdayLabels <- format(seq(min(ubd), max(ubd), 1)[weekdays(seq(min(ubd), max(ubd), 1), abbreviate = FALSE) == "Saturday"], "%b %d")
datetags <- VD$poorWKL$datetag
H <- hist(as.Date(unlist(lapply(unique(datetags), function(bd) rep(as.character(bd), times = 100*length(unique(VD$poorWKL$vstation[datetags == bd])) / max(VD$poorWKL$nSP[datetags == bd]) )))),
          breaks = seq(min(ubd, datetags), to = max(ubd), by = 1), freq = TRUE, ylab = "Max proportion of\nunstable grid points (%)", col = "lightgray",
          xlim = c(min(ubd), max(ubd)), ylim = c(0, 100), xaxt = "n", xlab = "", main = "", xaxs = "i")
axis(1, at = Saturdays, labels = SaturdayLabels, las = 2)
axis(2, at = pretty(c(20, 40, 60, 80, 100)))
# abline(v = as.Date(VD$wkl$datetag), lty = "solid", col = "red")
# abline(v = unique(as.Date(VD$poorWKL$datetag)), lty = "dashed")
abline(h = c(20, 40, 60, 80, 100), lty = "dotted", col = "lightgray")

## CAPTWKL
## proportion histogram
# VD$captWKL$datetag <- as.Date(sapply(VD$captWKL$wkl_uuid, function(wid) VD$wkl$datetag[VD$wkl$wkl_uuid == wid]))
datetags <- VD$captWKL$datetag
H <- hist(as.Date(unlist(lapply(unique(datetags), function(bd) rep(as.character(bd), times = 100*length(unique(VD$captWKL$vstation[datetags == bd & VD$captWKL$pu >= 0.77])) /
                                                                     max(VD$vframe$nSP[VD$vframe$vf_uuid %in% VD$captWKL$vf_uuid[datetags == bd]], na.rm = TRUE) )))),
          breaks = seq(min(ubd, datetags), to = max(ubd), by = 1), freq = TRUE, col = "red", xaxt = "n",
          add = TRUE)
          # ylab = "Proportion of unstable grid points (%)",
          # xlim = c(min(ubd), max(ubd)), ylim = c(0, 100), xlab = "", main = "", xaxs = "i")

dev.off()





