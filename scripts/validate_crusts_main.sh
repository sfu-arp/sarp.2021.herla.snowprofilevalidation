#!/bin/bash

#SBATCH --account=def-phaegeli
#SBATCH --time=45:00:00  # 9h total for GNP
#SBATCH --mem-per-cpu=14800  # 7600 GNP, 11600 BYK, 14800 S2S
#SBATCH --ntasks=1
#SBATCH --job-name=validate_crusts
#SBATCH --output=%x-%j.out
#SBATCH --mail-user=fherla@sfu.ca
#SBATCH --mail-type=FAIL,BEGIN,END

# The following job script will start sarp.2021.herla.snowprofilevalidation::validate_crusts_main.R
#
# Author: Florian Herla
# Last modified: June 2023

## CHECKLIST before running on cedar
# * all (SARP) packages installed with recent version?
# * created wklvalidation database?
#   * loaded DB with studyArea?
#   * loaded WklList into wkl table?
# * Snow_DB contains all required profiles?

####################

module load python/3.7 scipy-stack/2021a r/4.2.2
export R_LIBS=$HOME/R_libs

Rscript ./validate_crusts_main.R
