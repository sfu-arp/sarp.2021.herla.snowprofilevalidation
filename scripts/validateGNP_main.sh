#!/bin/bash

#SBATCH --account=def-phaegeli
#SBATCH --time=25:00:00  # roughly 3h per GNP season
#SBATCH --mem-per-cpu=7600
#SBATCH --ntasks=1
#SBATCH --job-name=validateGNP
#SBATCH --output=%x-%j.out
#SBATCH --mail-user=fherla@sfu.ca
#SBATCH --mail-type=FAIL,BEGIN,END

# The following job script will start sarp.2021.herla.snowprofilevalidation::validateGNP_main.R
#
# Author: Florian Herla
# Last modified: March 2022

## CHECKLIST before running on cedar
# * all (SARP) packages installed with recent version?
# * created wklvalidation database?
#   * loaded DB with studyArea?
#   * loaded WklList into wkl table?
# * Snow_DB contains all required profiles?

####################

module load python/3.7 scipy-stack/2021a r/4.2.1
export R_LIBS=$HOME/R_libs

Rscript ./validateGNP_main.R
