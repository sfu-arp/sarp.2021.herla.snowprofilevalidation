## --- raw scatterplot agreementIndicators ----
EV <- readRDS("output/assessQualityOfcaptWKL/GNP_BYK_v02.rds")
EV <- EV[!is.na(EV$wkl_uuid), ]
WKL <- readRDS("data/VData_WKL_GNP_BYK.rds")


png("output/assessQualityOfcaptWKL/figures/captWKLIndicators_BYK_GNP.png", width = 1400, height = 2100)
par(mfrow = c(8, 1), mar = c(4.1, 4.1, 0.1, 2.1), cex = 1.2)
cp <- 2.5
wkldates <- plotCaptWKLIndicators(EV, "rho_llhd", WKL, sortBy = "dq", cex.points = cp)
EV_sorted <- EV[EV$wkldate %in% wkldates, ]
EV_sorted <- EV[order(as.double(factor(as.character(EV$wkldate), levels = as.character(wkldates)))), ]

par(mar = c(2.1, 4.1, 1.1, 2.1))
tmp <- plotCaptWKLIndicators(EV_sorted, "rho_sens", WKL, sortBy = NULL, xticklabels = FALSE, cex.points = cp)
tmp <- plotCaptWKLIndicators(EV_sorted, "rho_dist", WKL, sortBy = NULL, xticklabels = FALSE, cex.points = cp)
tmp <- plotCaptWKLIndicators(EV_sorted, "offset_maxes", WKL, sortBy = NULL, xticklabels = FALSE, cex.points = cp)
tmp <- plotCaptWKLIndicators(EV_sorted, "pu_max", WKL, sortBy = NULL, xticklabels = FALSE, cex.points = cp)
tmp <- plotCaptWKLIndicators(EV_sorted, "offset_totaltime", WKL, sortBy = NULL, xticklabels = FALSE, cex.points = cp)
tmp <- plotCaptWKLIndicators(EV_sorted, "lagA_20", WKL, sortBy = NULL, xticklabels = FALSE, cex.points = cp)
tmp <- plotCaptWKLIndicators(EV_sorted, "lagZ_20", WKL, sortBy = NULL, xticklabels = FALSE, cex.points = cp)
dev.off()

png("output/assessQualityOfcaptWKL/figures/captWKL_sens.png", width = 1400, height = 300)
par(mfrow = c(1, 1), mar = c(6.1, 4.1, 2.1, 2.1), cex = 1.2)
cp <- 2.5
wkldates <- plotCaptWKLIndicators(EV, "rho_sens", WKL, sortBy = "dq", cex.points = cp)
dev.off()

png("output/assessQualityOfcaptWKL/figures/captWKL_dist.png", width = 1400, height = 300)
par(mfrow = c(1, 1), mar = c(6.1, 4.1, 2.1, 2.1), cex = 1.2)
cp <- 2.5
wkldates <- plotCaptWKLIndicators(EV, "rho_dist", WKL, sortBy = "dq", cex.points = cp)
dev.off()

png("output/assessQualityOfcaptWKL/figures/captWKL_pumax.png", width = 1400, height = 300)
par(mfrow = c(1, 1), mar = c(6.1, 4.1, 2.1, 2.1), cex = 1.2)
cp <- 2.5
wkldates <- plotCaptWKLIndicators(EV, "pu_max", WKL, sortBy = "dq", cex.points = cp)
dev.off()

png("output/assessQualityOfcaptWKL/figures/captWKL_lagA_halfofmax.png", width = 1400, height = 300)
par(mfrow = c(1, 1), mar = c(6.1, 4.1, 2.1, 2.1), cex = 1.2)
cp <- 2.5
wkldates <- plotCaptWKLIndicators(EV, "lagA_halfofmax", WKL, sortBy = "dq", cex.points = cp)
dev.off()

png("output/assessQualityOfcaptWKL/figures/captWKL_lagZ_halfofmax.png", width = 1400, height = 300)
par(mfrow = c(1, 1), mar = c(6.1, 4.1, 2.1, 2.1), cex = 1.2)
cp <- 2.5
wkldates <- plotCaptWKLIndicators(EV, "lagZhalfofmax", WKL, sortBy = "dq", cex.points = cp)
dev.off()

