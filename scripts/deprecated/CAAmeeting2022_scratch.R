

## --- visual for PWLcapturedByInidvidualProfile ----
snow19 <- readRDS("~/documents/sfu/code/rpackages_SARP/sarp.2021.herla.snowprofilevalidation/snowpacksimulations/data/snowpack_gnp_2019_ALP_allangles_PM.rds")
SPx <- (snow19$profiles[snow19$meta$station_id == "VIR0770034" & snow19$meta$date > "2018-11-09" & snow19$meta$date < "2018-12-10"])
meta <- (snow19$meta[snow19$meta$station_id == "VIR0770034" & snow19$meta$date > "2018-11-09" & snow19$meta$date < "2018-12-10", ])
SPx <- deriveDatetag(SPx)
idx_layerpool_rigid <- lapply(SPx, function(sp) findPWL(sp, pwl_gtype = grainDict$gtype, pwl_date = "2018-11-21", date_range = c(-10, 0), bdate_range = c(-1, 3)))
idx_layerpool_wx <- lapply(SPx, function(sp) findPWL(sp, pwl_gtype = grainDict$gtype, pwl_date = "2018-11-21", date_range = c(-5, 0), bdate_range = c(-1, 1)))

plot(SPx, Timeseries_labels = NA, ylim = c(90, 175), xaxs = "i")
abline(v = c(as.Date("2018-11-21")), lty ="dashed", col = "gray70")
lines(rep(as.Date("2018-11-11")-0.5, 2), c(154, 156), lty = "dashed")
lines(rep(as.Date("2018-11-21")+0.5, 2), c(154, 156))
lines(c(as.Date("2018-11-11")-0.5, as.Date("2018-11-21")+0.5), c(155, 155), lty = "dashed")
lines(c(as.Date("2018-11-16")-0.5, as.Date("2018-11-21")+0.5), c(155, 155), lwd = 1.5)
lines(rep(as.Date("2018-11-16")-0.5, 2), c(154, 156))

lines(rep(as.Date("2018-11-24")+0.5, 2), c(164, 166), lty = "dashed")
lines(rep(as.Date("2018-11-20")-0.5, 2), c(164, 166))
lines(c(as.Date("2018-11-20")-0.5, as.Date("2018-11-24")+0.5), c(165, 165), lty = "dashed")
lines(c(as.Date("2018-11-20")-0.5, as.Date("2018-11-22")+0.5), c(165, 165), lwd = 1.5)
lines(rep(as.Date("2018-11-22")+0.5, 2), c(164, 166))

for (l in seq_along(idx_layerpool_rigid)) {
  if (length(idx_layerpool_rigid[[l]]) > 0) rect(meta$date[l]-0.5, SPx[[l]]$layers$height[min(idx_layerpool_rigid[[l]])-1],
                                                 meta$date[l]+0.5, SPx[[l]]$layers$height[max(idx_layerpool_rigid[[l]])], col = "gray70", border = NA, density = 10)
}
for (l in seq_along(idx_layerpool_wx)) {
  if (length(idx_layerpool_wx[[l]]) > 0) rect(meta$date[l]-0.5, SPx[[l]]$layers$height[min(idx_layerpool_wx[[l]])-1],
                                              meta$date[l]+0.5, SPx[[l]]$layers$height[max(idx_layerpool_wx[[l]])], col = "transparent", border = "black")
}




## --- avgSP visuals ----
library(sarp.snowprofile.alignment)
avgSP4d <- readRDS("~/documents/sfu/code/rlocal_projects/similarityMeasure/data/gnp/averageGNPseason/avgGNP_v4d.rds")

## seasonal average until Jan 26:
png(filename = "/home/flo/documents/sfu/code/rlocal_projects/outreach/CAA_2022/seasonalAvg_GNP_Jan262019", width = 1500, height = 600)
par(cex.lab = 3, cex.axis = 2.5, mar = c(8.1, 8.1, 2.1, 2.1))
plot(avgSP4d$avgs[avgSP4d$meta$date <= "2019-01-26"], box = FALSE, xaxs = "i", yaxs = "i", ylab = "")
mtext("Height (cm)", side = 2, line = 5, cex = 3)
dev.off()

## Jan 26 hardness profile
png(filename = "/home/flo/documents/sfu/code/rlocal_projects/outreach/CAA_2022/avg_GNP_Jan262019.png", height = 600, width = 370)
par(mar = c(0.1, 0.1, 0.1, 0.1))
plot(avgSP4d$avgs$`2019-01-26`, axes = FALSE)
dev.off()

## lemons barplot
Jan26 <- snowprofileSet(lapply(avgSP4d$sets$`2019-01-26`, function(sp) {
  computeTSA(sp)
}))
# btLyrs <- backtrackLayers(avgSP4d$avgs$`2019-01-26`, profileSet = Jan26, condition = substitute(gtype %in% return_conceptually_similar_gtypes(as.character(avgProfile$layers$gtype[lidx]))))
btLyrs_all <- backtrackLayers(avgSP4d$avgs$`2019-01-26`, profileSet = Jan26)

## nL with TSA >= 5
nlTSA5 <- (do.call("c", lapply(btLyrs_all, function(df) {
  sum(df$tsa >= 5)
})))

## nltsa5 barplot:
png(filename = "/home/flo/documents/sfu/code/rlocal_projects/outreach/CAA_2022/avg_GNP_Jan262019_lemons.png", height = 600, width = 370)
par(cex = 1.5, cex.lab = 1.5, cex.axis = 1.2, mar = c(5.1, 4.1, 4.1, 2.1))
barplot(-nlTSA5/length(avgSP4d$sets$`2019-01-26`), width = c(avgSP4d$avgs$`2019-01-26`$layers$height[1], diff(avgSP4d$avgs$`2019-01-26`$layers$height)), col = "black",
        horiz = TRUE, border = NA, space = 0, ylim = c(0, avgSP4d$avgs$`2019-01-26`$hs), xlim = c(-1, 0), axes = FALSE)
axis(1, at = -1*c(0, 0.2, 0.4, 0.6, 0.8, 1), labels = c(0, 0.2, 0.4, 0.6, 0.8, 1))
# axis(2, at = seq(20, 120, by = 20))
mtext("Grid points with \n>= 5 lemons (%)", side = 1, line = 3.8, cex = 2)
grid()
dev.off()


## plot avgSP incl ppu_all
avgSP <- readRDS("~/documents/sfu/code/rpackages_SARP/sarp.2021.herla.snowprofilevalidation/output/averageProfiles/GNP2019_TL.rds")
avgSP$avgs <- snowprofileSet(lapply(avgSP$avgs, function(avg) {
  avg$layers$percentage <- avg$layers$ppu_all
  avg
}))

png(filename = "/home/flo/documents/sfu/code/rlocal_projects/outreach/CAA_2022/seasonalAvg_GNP_Jan262019_ppuall.png", width = 1500, height = 600)
par(cex.lab = 3, cex.axis = 2.5, mar = c(8.1, 8.1, 2.1, 2.1))
plot(avgSP$avgs[avgSP$meta$date >= xdaterange[1] & avgSP$meta$date <= as.Date("2019-01-26")], colAlpha = 0.5, xaxs = "i", yaxs = "i", ylab = "", box = FALSE)
plot(avgSP$avgs[avgSP$meta$date >= xdaterange[1] & avgSP$meta$date <= as.Date("2019-01-26")], ColParam = "percentage", add = TRUE, ylab = "", box = FALSE)
mtext("Height (cm)", side = 2, line = 5, cex = 3)
dev.off()




## --- CAA composite GNP 2019 ----

library(SarpBulletinTools)
library(sarp.2021.herla.snowprofilevalidation)
Bulletins <- readRDS("~/documents/sfu/code/rpackages_SARP/sarp.2021.herla.snowprofilevalidation/data/bulletins_GNP_for_simon_2013-2020.rds")
# ProblemTypesFull <- SarpBulletinTools::ListAvProblemTypes
# ProblemTypes <- SarpBulletinTools::ListAvProblemTypesAbbrev

## Get all bulletins
# load(url("http://data.avalancheresearch.ca/AllBulletins_2010To2022.RData"))
# load("~/documents/sfu/code/rpackages_SARP/sarp.2021.herla.snowprofilevalidation/data/AllBulletins_2010To2022.RData")
summary(Bulletins)
## Filtering out bulletins for 2019 and GNP
BullUUIDs <- Bulletins$Bulletins$BULLETIN_ID[Bulletins$Bulletins$SEASON == 2019 & Bulletins$Bulletins$REGION == "Glacier"]
BullGNP <- extractFromBulletinObj(Bulletins, ByBulletinID = BullUUIDs)
summary(BullGNP)

VD <- readRDS("~/documents/sfu/code/rpackages_SARP/sarp.2021.herla.snowprofilevalidation/data/VData_GNP_2019_TL.rds")

xdaterange <- c(as.Date("2018-11-06"), as.Date("2019-03-31"))

## FIG danger rating and WKLs
png(filename = "/home/flo/documents/sfu/code/rlocal_projects/outreach/CAA_2022/DngWKL_GNP2019_TL.png", width = 1800, height = 400)
layout(matrix(seq(2), ncol = 1), heights = c(1, 2))
par(xaxs = "i", cex = 1.5, cex.lab = 1.5)
## Plot danger ratings and problems
par(mar = c(1.1, 5.1, 0.5, 1.1))
par(bty = "n")
plotTSHzdRatingAvProb(BullGNP, ElevBand = "Tl", HighlightAvProb = c("STORM", "PERS", "DPERS"), plotAvProb = FALSE , DateStart = xdaterange[1], DateEnd = xdaterange[2], WithXAxis = FALSE)
par(mar = c(4.1, 5.1, 0.5, 1.1))
plotSeasonalOverviewProblemsPoorWKL(VD, xlim = xdaterange, showPWLs = TRUE, bty ="n", shadeAvProbPeriods = FALSE, pwcex = 1)
# shadeAvProbPeriod(BullGNP)
dev.off()


# ## FIG seasonalAvg cropped to xdaterange
# png(filename = "/home/flo/documents/sfu/code/rlocal_projects/outreach/CAA_2022/seasonalAvg_GNP_xdaterange", width = 1800, height = 600)
# par(cex.lab = 2, cex.axis = 2, mar = c(8.1, 8.1, 2.1, 2.1))
# plot(avgSP4d$avgs[avgSP4d$meta$date >= xdaterange[1] & avgSP4d$meta$date <= xdaterange[2]], box = FALSE, xaxs = "i", yaxs = "i", ylab = "")
# mtext("Height (cm)", side = 2, line = 5, cex = 2)
# dev.off()



## FIG danger rating and WKLs and captured WKLs
png(filename = "/home/flo/documents/sfu/code/rlocal_projects/outreach/CAA_2022/DngWKL_capt_GNP2019_TL.png", width = 1800, height = 660)
layout(matrix(seq(3), ncol = 1), heights = c(1, 2, 2))
par(xaxs = "i")

## Plot danger ratings and problems
par(mar = c(1.1, 5.1, 0.5, 1.1))
par(bty = "n")
plotTSHzdRatingAvProb(BullGNP, ElevBand = "Tl", HighlightAvProb = c("STORM", "PERS", "DPERS"), plotAvProb = FALSE , DateStart = xdaterange[1], DateEnd = xdaterange[2], WithXAxis = FALSE)
par(mar = c(4.1, 5.1, 0.5, 1.1))
plotSeasonalOverviewProblemsPoorWKL(VD, xlim = xdaterange, showPWLs = TRUE, bty ="n", shadeAvProbPeriods = FALSE, pwcex = 1)
shadeAvProbPeriod(BullGNP)
par(mar = c(1.1, 5.1, 0.5, 1.1))
plotSeasonalOverviewProblemsPoorWKL(VD, xlim = xdaterange, xaxt = "n" , withXAxis = TRUE)
dev.off()


## FIG 4 panel composite (structural)
png(filename = "/home/flo/documents/sfu/code/rlocal_projects/outreach/CAA_2022/DngWklAvg.png", width = 1800, height = 900)
layout(matrix(seq(4), ncol = 1), heights = c(1, 2.5, 3, 5))
par(xaxs = "i")

## Plot danger ratings and problems
par(mar = c(0.5, 5.1, 0.5, 1.1))
par(bty = "n", cex = 1.5)
plotTSHzdRatingAvProb(BullGNP, ElevBand = "Tl", HighlightAvProb = c("STORM", "PERS", "DPERS"), plotAvProb = FALSE , DateStart = xdaterange[1], DateEnd = xdaterange[2], WithXAxis = FALSE)
# par(mar = c(1.1, 5.1, 0.5, 1.1))
# plotTSAvProbLikelihood(BullGNP, ElevBand = "Tl", withXAxis = FALSE, xlim = xdaterange)
# abline(h = c(0, 2, 4, 6, 8), lty = "dotted", col = "gray70")
# axis(side = 2, line = 0, at = c(0, 2, 4, 6, 8), labels = c("Unlikely", "Possible", "Likely", "Very likely", "Almost certain"), las = 2)

## plot WKL stability overview
par(mar = c(4.1, 5.1, 0.5, 1.1))
plotSeasonalOverviewProblemsPoorWKL(VD, xlim = xdaterange, showPWLs = TRUE, bty ="n", shadeAvProbPeriods = FALSE, pwcex = 1.1)
# shadeAvProbPeriod(BullGNP)
par(mar = c(1.1, 5.1, 0.5, 1.1))
plotSeasonalOverviewProblemsPoorWKL(VD, xlim = xdaterange, xaxt = "n" , shadeAvProbPeriods = FALSE, plotBars = FALSE, withXAxis = FALSE)
abline(h = 0)

## plot avgSP4d
par(mar = c(4.1, 5.1, 0.1, 1.1))
plot(avgSP4d$avgs[avgSP4d$meta$date >= xdaterange[1]+1 & avgSP4d$meta$date <= xdaterange[2]], box = FALSE, xaxs = "i", yaxs = "i", ylab = "")
mtext("Height (cm)", side = 2, line = 3, cex = 1.5)
dev.off()


## FIG 4 panel composite (stability)
png(filename = "/home/flo/documents/sfu/code/rlocal_projects/outreach/CAA_2022/DngWklAvg_ppuall.png", width = 1800, height = 900)
layout(matrix(seq(4), ncol = 1), heights = c(1, 2.5, 3, 5))
par(xaxs = "i", cex = 1.5)

## Plot danger ratings and problems
par(mar = c(0.5, 5.1, 0.5, 1.1))
par(bty = "n")
plotTSHzdRatingAvProb(BullGNP, ElevBand = "Tl", HighlightAvProb = c("STORM", "PERS", "DPERS"), plotAvProb = FALSE , DateStart = xdaterange[1], DateEnd = xdaterange[2], WithXAxis = FALSE)
# par(mar = c(1.1, 5.1, 0.5, 1.1))
# plotTSAvProbLikelihood(BullGNP, ElevBand = "Tl", withXAxis = FALSE, xlim = xdaterange)
# abline(h = c(0, 2, 4, 6, 8), lty = "dotted", col = "gray70")
# axis(side = 2, line = 0, at = c(0, 2, 4, 6, 8), labels = c("Unlikely", "Possible", "Likely", "Very likely", "Almost certain"), las = 2)

## plot WKL stability overview
par(mar = c(4.1, 5.1, 0.5, 1.1))
plotSeasonalOverviewProblemsPoorWKL(VD, xlim = xdaterange, showPWLs = TRUE, bty ="n", shadeAvProbPeriods = FALSE, pwcex = 1.1)
# shadeAvProbPeriod(BullGNP)
par(mar = c(1.1, 5.1, 0.5, 1.1))
plotSeasonalOverviewProblemsPoorWKL(VD, xlim = xdaterange, xaxt = "n" , shadeAvProbPeriods = FALSE, plotBars = FALSE, withXAxis = FALSE)
abline(h = 0)

## plot avgSP with ppu_all
par(mar = c(4.1, 5.1, 0.1, 1.1))
plot(avgSP$avgs[avgSP$meta$date >= xdaterange[1]+1 & avgSP$meta$date <= xdaterange[2]], colAlpha = 0.5, xaxs = "i", yaxs = "i", box = FALSE)
plot(avgSP$avgs[avgSP$meta$date >= xdaterange[1]+1 & avgSP$meta$date <= xdaterange[2]], ColParam = "percentage", add = TRUE, box = FALSE)
mtext("Height (cm)", side = 2, line = 3, cex = 1.5)
dev.off()


## FIG 4 panel composite
# par(mfrow = c(5, 1), xaxs = "i")
png(filename = "output/figures/scratchCAA_2019.png", width = 1800, height = 900)
layout(matrix(seq(5), ncol = 1), heights = c(1, 2, 2, 3, 5))
par(xaxs = "i")

## Plot danger ratings and problems
par(mar = c(1.1, 5.1, 0.5, 1.1))
par(bty = "n")
plotTSHzdRatingAvProb(BullGNP, ElevBand = "Tl", HighlightAvProb = c("STORM", "PERS", "DPERS"), plotAvProb = FALSE , DateStart = xdaterange[1], DateEnd = xdaterange[2], WithXAxis = FALSE)
par(mar = c(1.1, 5.1, 0.5, 1.1))
plotTSAvProbLikelihood(BullGNP, ElevBand = "Tl", withXAxis = FALSE, xlim = xdaterange)
abline(h = c(0, 2, 4, 6, 8), lty = "dotted", col = "gray70")
axis(side = 2, line = 0, at = c(0, 2, 4, 6, 8), labels = c("Unlikely", "Possible", "Likely", "Very likely", "Almost certain"), las = 2)

## plot WKL stability overview
par(mar = c(4.1, 5.1, 0.5, 1.1))
plotSeasonalOverviewProblemsPoorWKL(VD, xlim = xdaterange, showPWLs = TRUE, bty ="n", shadeAvProbPeriods = FALSE, pwcex = 1)
shadeAvProbPeriod(BullGNP)
par(mar = c(1.1, 5.1, 0.5, 1.1))
plotSeasonalOverviewProblemsPoorWKL(VD, xlim = xdaterange, xaxt = "n" , withXAxis = FALSE)

## plot avgSP incl ppu_all
avgSP <- readRDS("~/documents/sfu/code/rpackages_SARP/sarp.2021.herla.snowprofilevalidation/output/averageProfiles/GNP2019_TL.rds")

par(mar = c(4.1, 5.1, 0.1, 1.1))
plot(avgSP$avgs[avgSP$meta$date >= xdaterange[1] & avgSP$meta$date <= xdaterange[2]], colAlpha = 0.3, xaxs = "i", yaxs = "i")
shadeAvProbPeriod(BullGNP)
plot(avgSP$avgs[avgSP$meta$date >= xdaterange[1] & avgSP$meta$date <= xdaterange[2]], colAlpha = 0.3, xaxs = "i", yaxs = "i", add = TRUE)
lines(avgSP$meta$date, avgSP$meta$hs_median, lwd = 1)
## median thickness of new snow
medianThicknessNewSnow <- sapply(avgSP$sets, function(set) {
  median(sapply(set, function(sp) {
    sum(sp$layers$thickness[findPWL(sp, pwl_gtype = c("PP", "DF"))])
  }))
})
lines(avgSP$meta$date, avgSP$meta$hs_median - medianThicknessNewSnow, lty = "dashed", lwd = 1)
# rect(as.Date(OUT[, 1])-0.5, ybottom, as.Date(OUT[, 1])+0.5, OUT[, 2], col = getColoursPercentage(as.numeric(OUT[, 3])), border = NA)
avgSP$avgs <- snowprofileSet(lapply(avgSP$avgs, function(avg) {
  avg$layers$percentage <- avg$layers$ppu_all
  avg
}))
plot(avgSP$avgs[avgSP$meta$date >= xdaterange[1] & avgSP$meta$date <= xdaterange[2]], ColParam = "percentage", add = TRUE)
dev.off()





## --- CAA composite GNP 2016 ----

library(SarpBulletinTools)
library(sarp.2021.herla.snowprofilevalidation)
Bulletins <- readRDS("~/documents/sfu/code/rpackages_SARP/sarp.2021.herla.snowprofilevalidation/data/bulletins_GNP_for_simon_2013-2020.rds")
# ProblemTypesFull <- SarpBulletinTools::ListAvProblemTypes
# ProblemTypes <- SarpBulletinTools::ListAvProblemTypesAbbrev

## Get all bulletins
# load(url("http://data.avalancheresearch.ca/AllBulletins_2010To2022.RData"))
summary(Bulletins)
## Filtering out bulletins for 2016 and GNP
BullUUIDs <- Bulletins$Bulletins$BULLETIN_ID[Bulletins$Bulletins$SEASON == 2016 & Bulletins$Bulletins$REGION == "Glacier"]
BullGNP <- extractFromBulletinObj(Bulletins, ByBulletinID = BullUUIDs)
summary(BullGNP)


xdaterange <- c(as.Date("2015-11-06"), as.Date("2016-03-31"))
# xdaterange <- c(as.Date("2018-11-06"), as.Date("2019-03-31"))
# par(mfrow = c(5, 1), xaxs = "i")
png(filename = "output/figures/scratchCAA.png", width = 1800, height = 900)
layout(matrix(seq(5), ncol = 1), heights = c(1, 2, 2, 3, 5))
par(xaxs = "i")

## Plot danger ratings and problems
par(mar = c(1.1, 5.1, 0.5, 1.1))
par(bty = "n")
plotTSHzdRatingAvProb(BullGNP, ElevBand = "Tl", HighlightAvProb = c("STORM", "PERS", "DPERS"), plotAvProb = FALSE , DateStart = xdaterange[1], DateEnd = xdaterange[2], WithXAxis = FALSE)
par(mar = c(1.1, 5.1, 0.5, 1.1))
plotTSAvProbLikelihood(BullGNP, ElevBand = "Tl", withXAxis = FALSE, xlim = xdaterange)
abline(h = c(0, 2, 4, 6, 8), lty = "dotted", col = "gray70")
axis(side = 2, line = 0, at = c(0, 2, 4, 6, 8), labels = c("Unlikely", "Possible", "Likely", "Very likely", "Almost certain"), las = 2)

## plot WKL stability overview
VD <- readRDS("~/documents/sfu/code/rpackages_SARP/sarp.2021.herla.snowprofilevalidation/data/VData_GNP_2016_TL.rds")
par(mar = c(4.1, 5.1, 0.5, 1.1))
plotSeasonalOverviewProblemsPoorWKL(VD, xlim = xdaterange, showPWLs = TRUE, bty ="n")
par(mar = c(1.1, 5.1, 0.5, 1.1))
plotSeasonalOverviewProblemsPoorWKL(VD, xlim = xdaterange, xaxt = "n" , withXAxis = FALSE)

## plot avgSP incl ppu_all
avgSP <- readRDS("~/documents/sfu/code/rpackages_SARP/sarp.2021.herla.snowprofilevalidation/output/averageProfiles/GNP2016_TL.rds")
OUT <- readRDS("output/averageProfiles/GNP2016_TL_percentagePU.rds")
ybottom <- OUT[, 2]
ybottom[which(diff(as.numeric(OUT[, 2])) < 0)] <- 0
ybottom <- c(0, ybottom[-length(ybottom)])

par(mar = c(4.1, 5.1, 0.1, 1.1))
plot(avgSP$avgs[avgSP$meta$date >= xdaterange[1] & avgSP$meta$date <= xdaterange[2]], colAlpha = 0.3, xaxs = "i", yaxs = "i")
shadeAvProbPeriod(BullGNP)
plot(avgSP$avgs[avgSP$meta$date >= xdaterange[1] & avgSP$meta$date <= xdaterange[2]], colAlpha = 0.3, xaxs = "i", yaxs = "i", add = TRUE)
lines(avgSP$meta$date, avgSP$meta$hs_median, lwd = 1)
## median thickness of new snow
medianThicknessNewSnow <- sapply(avgSP$sets, function(set) {
  median(sapply(set, function(sp) {
    sum(sp$layers$thickness[findPWL(sp, pwl_gtype = c("PP", "DF"))])
  }))
})
lines(avgSP$meta$date, avgSP$meta$hs_median - medianThicknessNewSnow, lty = "dashed", lwd = 1)
rect(as.Date(OUT[, 1])-0.5, ybottom, as.Date(OUT[, 1])+0.5, OUT[, 2], col = getColoursPercentage(as.numeric(OUT[, 3])), border = NA)
# avgSP$avgs <- snowprofileSet(lapply(avgSP$avgs, function(avg) {
#   avg$layers$percentage <- avg$layers$ppu_all
#   avg
# }))
# plot(avgSP$avgs[avgSP$meta$date >= xdaterange[1] & avgSP$meta$date <= xdaterange[2]], ColParam = "percentage", add = TRUE, )
dev.off()

