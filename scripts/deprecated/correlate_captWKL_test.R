library(sarp.2021.herla.snowprofilevalidation)
VD <- readRDS("data/VData_GNP_2019_TL_ranks123.rds")

wuid <- "3A0724D8-4D25-8F30-53EC-4D69C1A30E57"  # Jan 17 SH 2019
wuid <- "B51A8C69-FB21-8571-3B57-17D9306F45EC"  # Dec 9 SH 2019

VF <- VD$vframe[VD$vframe$wkl_uuid == wuid & !VD$vframe$gtype_rank %in% "tertiary", ]
likelihood_rep <- VF$llhd_rep
proportion_unstable <- unname(sapply(VF$vf_uuid, function(vfuid) {
  length(unique(VD$captWKL$vstation_id[VD$captWKL$vf_uuid == vfuid & VD$captWKL$pu >= 0.77]))/VF$nSP[VF$vf_uuid == vfuid]
}))
distribution_rep <- as.numeric(VF$dist_rep)
sensitivity_rep <- as.numeric(VF$sens_rep)

par(mfrow = c(3, 1))
corcoeff <- cor.test(likelihood_rep, proportion_unstable, method = "spearman")
plot(likelihood_rep, proportion_unstable, type = "p", main = paste0("rho = ", round(corcoeff$estimate, 2), ", p = ", round(corcoeff$p.value, 3)))
corcoeff <- cor.test(distribution_rep, proportion_unstable, method = "spearman")
plot(distribution_rep, proportion_unstable, type = "p", main = paste0("rho = ", round(corcoeff$estimate, 2), ", p = ", round(corcoeff$p.value, 3)))
corcoeff <- cor.test(sensitivity_rep, proportion_unstable, method = "spearman")
plot(sensitivity_rep, proportion_unstable, type = "p", main = paste0("rho = ", round(corcoeff$estimate, 2), ", p = ", round(corcoeff$p.value, 3)))

