
# sarp.2021.herla.snowprofilevalidation


The R package `sarp.2021.herla.snowprofilevalidation` contains the entire analysis of Florian's PhD project of validating snowpack simulations for their capability to model critical 
avalanche layers.

## Folder structure

  * `data/`: rather small R data files, e.g.
      - Weak layer tracking data (`WklList_GNP_2012-2021.rds`)
  
  * `data-raw/`: currently no data used for analysis
      - Profiles from South Rockies field team 2021 incl. read routine
      
  * `output/`
  
  * `R/`: functions used during analysis
  
  * `scripts/`: analysis and exploratory scripts
      - `prepareWkl_GNP.R` generates weak layer tracking data file from above
      
  * `snowpacksimulations/`
      - `./data`: only stored locally (not synced to bitbucket) due to large file sizes
      - `./gnp`: all files required to setup SNOWPACK, run simulations, and download data from cedar mysql database

